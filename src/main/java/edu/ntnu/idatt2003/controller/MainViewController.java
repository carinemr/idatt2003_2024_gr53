package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGame;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.fileHandling.ChaosGameFileHandler;
import edu.ntnu.idatt2003.model.observer.ChaosGameObserver;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import edu.ntnu.idatt2003.view.uiBoxes.CustomFractalCreationBox;
import edu.ntnu.idatt2003.view.uiBoxes.DashboardBox;
import edu.ntnu.idatt2003.view.uiBoxes.EditLiveFractalBox;
import edu.ntnu.idatt2003.view.uiBoxes.FractalDisplayBox;
import edu.ntnu.idatt2003.view.uiBoxes.HelpBox;
import edu.ntnu.idatt2003.view.MainView;
import edu.ntnu.idatt2003.view.uiBoxes.MenuBox;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller class for the main view, that performs
 * the communication between the view and the model,
 * as well as setting what actions are to be performed
 * by the elements in the view.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class MainViewController implements ChaosGameObserver {
  private final int CANVAS_WIDTH = 900;
  private final int CANVAS_HEIGHT = 700;
  private final int BASE_STEPS = 100000;
  private final String MSG_ERROR_INVALID_INPUT = "Could not create the fractal due to the variables" +
      " being invalid.";
  private final String MSG_ERROR_STEPS = "Please pick a lower amount of steps, as the current will " +
      "likely cause the application to crash.";
  private final String MSG_COULD_NOT_READ_FILE = "Could not create a fractal from the given file.";
  private final String MSG_INVALID_FILE_TYPE = "Please make sure that the file is a .txt-file.";
  private final String MSG_EMPTY = "";
  private final MainView mainView;
  private final MenuBox menuBox;
  private final DashboardBox dashboardBox;
  private final HelpBox helpBox;
  private final FractalDisplayBox fractalDisplayBox;
  private final EditLiveFractalBox editLiveFractalBox;
  private final CustomFractalCreationBox fractalCreationBox;
  private final RadioButton btnAffine2D;
  private final RadioButton btnJulia;
  private final Button btnAddAnother;
  private final Button btnShow;
  private RadioButton selectedButton;
  private ChaosGame chaosGame;
  private int currentSteps;

  /**
   * Method that creates every box and button that
   * that will have action performed to or on them.
   *
   * @param mainView main view of the controller
   * @throws FileNotFoundException if logo or run logos are not found
   */
  public MainViewController(MainView mainView) throws FileNotFoundException {
    this.mainView = mainView;
    menuBox = new MenuBox();
    dashboardBox = new DashboardBox();
    helpBox = new HelpBox();
    fractalDisplayBox = new FractalDisplayBox();
    editLiveFractalBox = new EditLiveFractalBox();
    fractalCreationBox = new CustomFractalCreationBox();
    btnAffine2D = fractalCreationBox.getBtnAffine2D();
    btnJulia = fractalCreationBox.getBtnJulia();
    btnAddAnother = fractalCreationBox.getBtnAddAnotherTransformation();
    btnShow = fractalCreationBox.getBtnShowFractal();
    chaosGame = null;
    init();
  }

  /**
   * Method that initializes the elements to be
   * initially displayed in the application, as well
   * as setting the action for the buttons.
   */
  public void init() {
    setLeftBox(menuBox.getMenuBox());
    setRightBox(dashboardBox.getDashboardBox());

    setActionOnMenuButtons();
    setActionOnDashboardButtons();
    setActionOnEditFractalButtons();
    setActionOnCreateFractalButtons();

    setActionOnEditFractalButtons();

    fractalCreationBox.getFractalCreationBox().prefWidthProperty().bind(mainView.getRightBox().widthProperty());
  }

  /**
   * Get-method to retrieve the root box, that contains
   * the left and right box from the mainView.
   *
   * @return contents of root box
   */
  private HBox getRoot() {
    return mainView.getRoot();
  }

  /**
   * Get-method to retrieve the content of the left box
   * from the mainView.
   *
   * @return contents of left box
   */
  private VBox getLeftBox() {
    return mainView.getLeftBox();
  }

  /**
   * Get-method to retrieve the content of
   * the right box from the mainView.
   *
   * @return contents of right box
   */
  private VBox getRightBox() {
    return mainView.getRightBox();
  }

  /**
   * Method that overrides the descriptionUpdate-method
   * from the ChaosGameObserver interface. When notified,
   * the runSteps-method is called.
   */
  @Override
  public void descriptionUpdate() {
    runSteps();
  }

  /**
   * Method that overrides the canvasUpdate-method from the ChaosGameObserver interface.
   * When notified, a new canvas array is created, and the canvas is shown with the new values.
   * ChatGPT helped with the ability to turn the array-canvas into an ImageView, by using
   * WriteableImage and PixelWriter. ChatGPT was also asked on how to use the PixelWriter,
   * and the suggested code was implemented. Later, the ability to check the value of
   * the element of the canvas-array was implemented. To implement the blue color, ChatGPT
   * was asked, and suggested using bits for mapping the RGB spectrum.
   */
  @Override
  public void canvasUpdate() {
    int[][] canvasArray = chaosGame.getCanvas().getCanvasArray();
    int width = canvasArray[0].length;
    int height = canvasArray.length;

    WritableImage writableImage = new WritableImage(width, height);
    PixelWriter pixelWriter = writableImage.getPixelWriter();

    for (int i = 0; i < (height); i++) {
      for (int j = 0; j < (width); j++) {
        Color color;
        int value = canvasArray[i][j];
        if (value == 0) {
          color = Color.TRANSPARENT;
        } else if (value > 255){
          color = Color.BLACK;
        } else {
          int blue = (value & 0x03) << 6;
          color = Color.rgb(0, 0, blue);
        }

        pixelWriter.setColor(j, i, color);
      }
    }

    ImageView fractalImageView = fractalDisplayBox.getFractalImage();
    fractalImageView.setImage(writableImage);

    HBox root = getRoot();

    fractalImageView.setPreserveRatio(true);
    fractalImageView.fitWidthProperty().unbind();
    fractalImageView.fitHeightProperty().unbind();

    Insets paddingRightBox = getRightBox().getPadding();

    fractalImageView.fitWidthProperty().bind(root.widthProperty().subtract(
        getLeftBox().getWidth() + paddingRightBox.getLeft() + paddingRightBox.getRight()));
    fractalImageView.fitHeightProperty().bind(root.heightProperty().subtract(
        paddingRightBox.getTop() + paddingRightBox.getBottom()));

    editLiveFractalBox.setErrorMessageForUser(MSG_EMPTY);
  }

  /**
   * Method that calls on the runSteps-method from the
   * ChaosGame class, with the current steps of the
   * application as the parameter.
   */
  private void runSteps() {
    chaosGame.runSteps(currentSteps);
  }

  /**
   * Method to create the fractal based on a given description,
   * adds observer and runs the chaos game. Right box is set to
   * display the fractal, left box set to display the live edit
   * menu.
   *
   * @param chaosGameDescription description for the game
   */
  private void createFractal(ChaosGameDescription chaosGameDescription) {
    if(!(chaosGame == null)) {
      chaosGame.removeObserver(this);
    }
    chaosGame = new ChaosGame(chaosGameDescription, CANVAS_WIDTH, CANVAS_HEIGHT);
    chaosGame.addObserver(this);

    currentSteps = BASE_STEPS;
    chaosGame.runSteps(currentSteps);

    setRightBox(fractalDisplayBox.getFractalDisplayBox());
    setLeftBox(editLiveFractalBox.getEditBox());
    initializeTxtFieldValuesForEditFractal();

    menuBox.getBtnViewRecentFractal().getStyleClass().remove("inactiveButton");
    menuBox.getBtnViewRecentFractal().getStyleClass().add("menuBoxButton");
  }

  /**
   * Method to visualize fractal from a given
   * path parameter. If anything goes wrong,
   * an alert is shown.
   * @param path path for file to be visualized
   */
  private void visualizeFractalFromPath(String path) {
    try {
      ChaosGameDescription chaosGameDescription = ChaosGameFileHandler.readFromFile(path);
      createFractal(chaosGameDescription);
    } catch (Exception e) {
      mainView.showAlert(MSG_COULD_NOT_READ_FILE, Alert.AlertType.ERROR);
    }
  }

  /**
   * Method to create a custom game description from
   * the textfields on the 'create new fractal'-page.
   * The type of transforms that are created is based on
   * which radio button, julia or affine2D, that was
   * clicked.
   *
   * @return custom game description
   */
  public ChaosGameDescription createCustomGameDescription() {
    List<TextField> minMaxValues = fractalCreationBox.getMinMaxTextFields();

    double x0min = Double.parseDouble(minMaxValues.get(0).getText());
    double x1min = Double.parseDouble(minMaxValues.get(1).getText());

    double x0max = Double.parseDouble(minMaxValues.get(2).getText());
    double x1max = Double.parseDouble(minMaxValues.get(3).getText());

    Vector2D minCoords = new Vector2D(x0min, x1min);
    Vector2D maxCoords = new Vector2D(x0max, x1max);

    List<Transform2D> transforms = new ArrayList<>();

    if (selectedButton.equals(btnAffine2D)) {
      transforms = createCustomAffine2dGame();
    } else if (selectedButton.equals(btnJulia)) {
      transforms = createCustomJuliaGame();
    }

    return new ChaosGameDescription(transforms, minCoords, maxCoords);
  }

  /**
   * Method that creates a list of custom affine2D transforms,
   * if the current selected button is affine2D.
   *
   * @return list of affine2D transforms
   */
  public List<Transform2D> createCustomAffine2dGame() {
    List<Transform2D> transforms = new ArrayList<>();

    for (List<TextField> valueList : fractalCreationBox.getAllTransformationTextFields()) {
      double a00 = Double.parseDouble(valueList.get(0).getText());
      double a01 = Double.parseDouble(valueList.get(1).getText());
      double a10 = Double.parseDouble(valueList.get(2).getText());
      double a11 = Double.parseDouble(valueList.get(3).getText());
      Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);

      double b1 = Double.parseDouble(valueList.get(4).getText());
      double b2 = Double.parseDouble(valueList.get(5).getText());
      Vector2D vector = new Vector2D(b1, b2);

      AffineTransform2D transform = new AffineTransform2D(matrix, vector);

      transforms.add(transform);
    }

    return transforms;
  }

  /**
   * Method that creates a list of custom julia transforms,
   * if the current selected button is julia, both for a
   * positive and negative sign.
   *
   * @return list of julia transforms
   */
  public List<Transform2D> createCustomJuliaGame() {
    List<Transform2D> transforms = new ArrayList<>();

    List<TextField> complexValues = fractalCreationBox.getComplexValueTextFields();
    double realPart = Double.parseDouble(complexValues.get(0).getText());
    double imaginaryPart = Double.parseDouble(complexValues.get(1).getText());

    Complex point = new Complex(realPart, imaginaryPart);

    JuliaTransform transformPos = new JuliaTransform(point, 1);
    JuliaTransform transformNeg = new JuliaTransform(point, -1);

    transforms.add(transformPos);
    transforms.add(transformNeg);

    return transforms;
  }

  /**
   * Method that sets the content of the left box,
   * after clearing any currently present
   * content first.
   *
   * @param box box to be set as left side content
   */
  private void setLeftBox(VBox box) {
    VBox leftBox = getLeftBox();

    leftBox.getChildren().clear();
    leftBox.getChildren().add(box);
  }

  /**
   * Method that sets the content of the right box,
   * after clearing any currently present
   * content first.
   *
   * @param box box to be set as right side content
   */
  private void setRightBox(VBox box) {
    VBox rightBox = getRightBox();

    rightBox.getChildren().clear();
    rightBox.getChildren().add(box);
  }


  /**
   * Method to set action to all menu buttons, so left
   * and/or right box contents are displayed according
   * to the button clicked.
   */
  private void setActionOnMenuButtons() {
    Button btnDashboard = menuBox.getBtnDashboard();
    Button btnCreateNewFractal = menuBox.getBtnCreateNewFractal();
    Button btnViewRecentFractal = menuBox.getBtnViewRecentFractal();
    Button btnHelp = menuBox.getBtnHelp();
    Button btnQuitApplication = menuBox.getBtnQuitApplication();

    btnDashboard.setOnAction(actionEvent -> setRightBox(dashboardBox.getDashboardBox()));

    btnCreateNewFractal.setOnAction(actionEvent -> setRightBox(fractalCreationBox.getRightSideCreationBox()));

    btnViewRecentFractal.setOnAction(actionEvent -> {
      if (!(chaosGame == null)) {
        setLeftBox(editLiveFractalBox.getEditBox());
        setRightBox(fractalDisplayBox.getFractalDisplayBox());
      }
    });

    btnHelp.setOnAction(actionEvent -> setRightBox(helpBox.getRightSideHelpBox()));

    btnQuitApplication.setOnAction(actionEvent -> Platform.exit());
  }

  /**
   * Method to set action to all dashboard buttons,
   * so the application runs the corresponding
   * fractal to the button, either pre-determined
   * or custom.
   */
  private void setActionOnDashboardButtons() {
    Button btnSierpinski = dashboardBox.getBtnSierpinski();
    Button btnBarnsleyFern = dashboardBox.getBtnBarnsleyFern();
    Button btnJulia = dashboardBox.getBtnJulia();
    Button btnUploadFile = dashboardBox.getBtnUploadFile();

    btnSierpinski.setOnAction(actionEvent -> {
      ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.getDescription("sierpinski");
      createFractal(chaosGameDescription);
    });

    btnBarnsleyFern.setOnAction(actionEvent -> {
      ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.getDescription("barnsley_fern");
      createFractal(chaosGameDescription);
    });

    btnJulia.setOnAction(actionEvent -> {
      ChaosGameDescription chaosGameDescription = ChaosGameDescriptionFactory.getDescription("julia");
      createFractal(chaosGameDescription);
    });

    btnUploadFile.setOnAction(actionEvent -> showFileChoosingDialog());
  }

  /**
   * Method to set action to all 'create new fractal'-buttons,
   * to ensure all fields are filled with valid values,
   * displays error message if not.
   */
  public void setActionOnCreateFractalButtons() {
    fractalCreationBox.getTransformationToggle().selectedToggleProperty().addListener(
        (observable, oldValue, newValue) -> {
          if (newValue != null) {
            selectedButton = (RadioButton) newValue;
            fractalCreationBox.setErrorMessage(MSG_EMPTY);
            if (selectedButton.equals(btnAffine2D)) {
              fractalCreationBox.fillAffine2DTransformationInputBox();
            } else if (selectedButton.equals(btnJulia)) {
              fractalCreationBox.fillJuliaTransformationInputBox();
            }
          }
        });

    btnAddAnother.setOnAction(actionEvent -> {
      fractalCreationBox.incrementTransformationIteration();
      fractalCreationBox.createAffine2DInputTextFields();
    });

    btnShow.setOnAction(actionEvent -> {
      try {
        ChaosGameDescription description = createCustomGameDescription();
        createFractal(description);
        fractalCreationBox.setErrorMessage(MSG_EMPTY);
      } catch (Exception e) {
        fractalCreationBox.setErrorMessage(MSG_ERROR_INVALID_INPUT);
      }
    });
  }

  /**
   * Method to set action to buttons on the 'edit fractal'-page.
   */
  private void setActionOnEditFractalButtons() {
    Button btnBackToDashboard = editLiveFractalBox.getBtnBackToDashboard();
    Button btnSaveSetToFile = editLiveFractalBox.getBtnSaveSetToFile();

    btnBackToDashboard.setOnAction(actionEvent -> {
      setLeftBox(menuBox.getMenuBox());
      setRightBox(dashboardBox.getDashboardBox());
    });

    btnSaveSetToFile.setOnAction(actionEvent -> showSaveFileDialog());
  }

  /**
   * Method that initializes the textfield values for
   * editing the fractal, and adds listeners.
   */
  public void initializeTxtFieldValuesForEditFractal() {
    editLiveFractalBox.getInputBox().getChildren().clear();

    editLiveFractalBox.addStepsInput();
    addListenerToSteps();

    editLiveFractalBox.addCoordsInput();
    addListenersToCoords();

    Transform2D firstTransformation = chaosGame.getDescription().getTransforms().getFirst();

    if (firstTransformation instanceof JuliaTransform) {
      editLiveFractalBox.addJuliaInput();
      addListenersToJuliaTransformations();
    } else if (firstTransformation instanceof AffineTransform2D) {
      editLiveFractalBox.clearListTxtFldAffine();
      List<Transform2D> currentTransformation = chaosGame.getDescription().getTransforms();

      for (int i = 0; i < currentTransformation.size(); i++) {
        editLiveFractalBox.addAffineInput(i+1);
        addListenersToAffineTransformation(i);
      }
    }
  }

  /**
   * Method that adds listeners to the textfield
   * for the steps, and checks if valid.
   */
  private void addListenerToSteps() {
    TextField txtFldSteps = editLiveFractalBox.getTxtFldSteps();

    txtFldSteps.setText("" + BASE_STEPS);

    txtFldSteps.textProperty().addListener((observableValue, oldValue, newValue) -> {
      if (validateSteps(txtFldSteps)) {
        currentSteps = Integer.parseInt(txtFldSteps.getText());
        runSteps();
      } else {
        editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_STEPS);
      }
    });
  }

  /**
   * Method that adds listeners to the min and
   * max coords textfields, and checks if valid.
   */
  private void addListenersToCoords() {
    List<TextField> txtFldListOfCoords = editLiveFractalBox.getListTxtFldCoords();

    ChaosGameDescription description = chaosGame.getDescription();

    TextField txtFldMinCoordsX0 = txtFldListOfCoords.get(0);
    TextField txtFldMinCoordsX1 = txtFldListOfCoords.get(1);
    TextField txtFldMaxCoordsX0 = txtFldListOfCoords.get(2);
    TextField txtFldMaxCoordsX1 = txtFldListOfCoords.get(3);

    Vector2D minCoords = description.getMinCoords();
    Vector2D maxCoords = description.getMaxCoords();

    txtFldMinCoordsX0.setText("" + minCoords.getX0());
    txtFldMinCoordsX1.setText("" + minCoords.getX1());
    txtFldMaxCoordsX0.setText("" + maxCoords.getX0());
    txtFldMaxCoordsX1.setText("" + maxCoords.getX1());


    txtFldMinCoordsX0.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      if (validateDecimalEditInput(txtFldMinCoordsX0)) {
        double minX0 = Double.parseDouble(txtFldMinCoordsX0.getText());
        try {
          Vector2D newMinCoords = new Vector2D(minX0, minCoords.getX1());
          chaosGame.setMinCoords(newMinCoords);
        } catch (Exception e) {
          editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
        }
      }
    }));

    txtFldMinCoordsX1.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      if (validateDecimalEditInput(txtFldMinCoordsX1)) {
        double minX1 = Double.parseDouble(txtFldMinCoordsX1.getText());
        try {
          Vector2D newMinCoords = new Vector2D(minCoords.getX0(), minX1);
          chaosGame.setMinCoords(newMinCoords);
        } catch (Exception e) {
          editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
        }
      }
    }));

    txtFldMaxCoordsX0.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      if (validateDecimalEditInput(txtFldMaxCoordsX0)) {
        double maxX0 = Double.parseDouble(txtFldMaxCoordsX0.getText());
        try {
          Vector2D newMaxCoords = new Vector2D(maxX0, maxCoords.getX1());
          chaosGame.setMaxCoords(newMaxCoords);
        } catch (Exception e) {
          editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
        }
      }
    }));

    txtFldMaxCoordsX1.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      if (validateDecimalEditInput(txtFldMaxCoordsX1)) {
        double maxX1 = Double.parseDouble(txtFldMaxCoordsX1.getText());
        try {
          Vector2D newMaxCoords = new Vector2D(maxCoords.getX0(), maxX1);
          chaosGame.setMaxCoords(newMaxCoords);
        } catch (Exception e) {
          editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
        }
      }
    }));
  }

  /**
   * Method that adds listeners to the affine2d
   * transform textfields, and checks if valid.
   */
  private void addListenersToAffineTransformation(int transformIndex) {
    List<TextField> listTxtFldAffineTransformation =
        editLiveFractalBox.getListTxtFldAffine().get(transformIndex);

    ChaosGameDescription description = chaosGame.getDescription();
    AffineTransform2D currentTransform =
        (AffineTransform2D) description.getTransforms().get(transformIndex);
    Matrix2x2 aMatrix = currentTransform.getMatrix();
    Vector2D bVector = currentTransform.getVector();

    TextField txtFldA00 = listTxtFldAffineTransformation.get(0);
    TextField txtFldA01 = listTxtFldAffineTransformation.get(1);
    TextField txtFldA10 = listTxtFldAffineTransformation.get(2);
    TextField txtFldA11 = listTxtFldAffineTransformation.get(3);
    TextField txtFldB0 = listTxtFldAffineTransformation.get(4);
    TextField txtFldB1 = listTxtFldAffineTransformation.get(5);

    txtFldA00.setText("" + aMatrix.getA00());
    txtFldA01.setText("" + aMatrix.getA01());
    txtFldA10.setText("" + aMatrix.getA10());
    txtFldA11.setText("" + aMatrix.getA11());
    txtFldB0.setText("" + bVector.getX0());
    txtFldB1.setText("" + bVector.getX1());

    listTxtFldAffineTransformation.forEach(textField -> {
      textField.textProperty().addListener((observableValue, oldValue, newValue) -> {
        if (validateDecimalEditInput(txtFldA00) && validateDecimalEditInput(txtFldA01) &&
            validateDecimalEditInput(txtFldA10) && validateDecimalEditInput(txtFldA11) &&
            validateDecimalEditInput(txtFldB0) && validateDecimalEditInput(txtFldB1)) {
          try {

            double a00 = Double.parseDouble(txtFldA00.getText());
            double a01 = Double.parseDouble(txtFldA01.getText());
            double a10 = Double.parseDouble(txtFldA10.getText());
            double a11 = Double.parseDouble(txtFldA11.getText());
            double b0 = Double.parseDouble(txtFldB0.getText());
            double b1 = Double.parseDouble(txtFldB1.getText());

            Matrix2x2 newMatrix = new Matrix2x2(a00, a01, a10, a11);
            Vector2D newVector = new Vector2D(b0, b1);
            Transform2D newTransform = new AffineTransform2D(newMatrix, newVector);

            chaosGame.setSpecificTransform(transformIndex, newTransform);
          } catch (Exception e) {
            editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
          }
        }
      });
    });
  }

  /**
   * Method that adds listeners to the julia
   * transform textfields, and checks if valid.
   */
  private void addListenersToJuliaTransformations() {
    List<TextField> listTxtFldJulia = editLiveFractalBox.getListTxtFldJulia();

    ChaosGameDescription description = chaosGame.getDescription();
    Complex currentC = ((JuliaTransform) description.getTransforms().get(0)).getPoint();

    TextField txtFieldCx0 = listTxtFldJulia.get(0);
    TextField txtFieldCx1 = listTxtFldJulia.get(1);

    txtFieldCx0.setText("" + currentC.getX0());
    txtFieldCx1.setText("" + currentC.getX1());

    txtFieldCx0.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      try {
        setNewJuliaTransforms(txtFieldCx0, txtFieldCx1);
      } catch (Exception e) {
        editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
      }
    }));

    txtFieldCx1.textProperty().addListener(((observableValue, oldValue, newValue) -> {
      try {
        setNewJuliaTransforms(txtFieldCx0, txtFieldCx1);
      } catch (Exception e) {
        editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
      }
    }));
  }

  /**
   * Method that sets new julia transforms from the given
   * parameters if they are valid.
   *
   * @param txtFldCx0 textfield for the x0 value
   * @param txtFldCx1 textfield for the x1 value
   */
  private void setNewJuliaTransforms(TextField txtFldCx0, TextField txtFldCx1) {
    if (validateDecimalEditInput(txtFldCx0) && validateDecimalEditInput(txtFldCx1)) {
      double cX0 = Double.parseDouble(txtFldCx0.getText());
      double cX1 = Double.parseDouble(txtFldCx1.getText());

      Complex c = new Complex(cX0, cX1);

      JuliaTransform transform1 = new JuliaTransform(c, -1);
      JuliaTransform transform2 = new JuliaTransform(c, 1);

      List<Transform2D> transforms = new ArrayList<>();
      transforms.add(transform1);
      transforms.add(transform2);

      chaosGame.setTransforms(transforms);
    } else {
      throw new IllegalArgumentException("The text-field values were invalid");
    }
  }

  /**
   * Method to validate the decimal edit input from
   * the given textfield parameter, and gives error
   * message if the field is empty.
   *
   * @param textField textfield to validate
   * @return true or false if valid or not
   */
  private boolean validateDecimalEditInput(TextField textField) {
    if (textField.getText().isEmpty()) {
      editLiveFractalBox.setErrorMessageForUser(MSG_ERROR_INVALID_INPUT);
      return false;
    } else if (!txtFldIsADecimal(textField)) {
      return false;
    }
    return true;
  }

  /**
   * Method to validate the steps from the given
   * textfield parameter, and gives error message
   * if the field is empty.
   *
   * @param textField textfield to validate
   * @return true or false if valid or not
   */
  private boolean validateSteps(TextField textField) {
    if (textField.getText().isEmpty() || !txtFldIsAPositiveInt(textField)) {
      return false;
    } else if (Double.parseDouble(textField.getText()) > 90000000) {
      return false;
    }
    return true;
  }

  /**
   * Method to validate if the textfield is a decimal
   * from the given textfield parameter.
   *
   * @param textField textfield to validate
   * @return true or false if valid or not
   */
  private boolean txtFldIsADecimal(TextField textField) {
    try {
      double number = Double.parseDouble(textField.getText());
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  /**
   * Method to validate if the textfield is a positive
   * integer from the given textfield parameter.
   *
   * @param textField textfield to validate
   * @return true or false if valid or not
   */
  private boolean txtFldIsAPositiveInt(TextField textField) {
    try {
      int number = Integer.parseInt(textField.getText());
      if (number < 0) {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  /**
   * Method to show the file chooser dialog, only allowing .txt files to be submitted. It shows
   * an alert in the MainView in case of an invalid file.
   */
  private void showFileChoosingDialog() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    fileChooser.getExtensionFilters().add(
        new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    String path = fileChooser.showOpenDialog(new Stage()).getAbsolutePath();

    if (!path.endsWith(".txt")) {
      mainView.showAlert(MSG_INVALID_FILE_TYPE, Alert.AlertType.ERROR);
    }

    visualizeFractalFromPath(path);
  }

  /**
   * Method to show a file chooser dialog, letting the user save the file to their chosen path
   * on their computer.
   */
  private void showSaveFileDialog() {
    FileChooser fileChooser =new FileChooser();
    fileChooser.setTitle("Save set to file");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("txt-files", "*.txt"));

    String path = fileChooser.showSaveDialog(new Stage()).getAbsolutePath();
    ChaosGameDescription description = chaosGame.getDescription();

    ChaosGameFileHandler.writeToFile(description, path);
  }
}