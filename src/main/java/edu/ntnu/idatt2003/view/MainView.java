package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.controller.MainViewController;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Class that calls on methods that builds the application view,
 * that extends Application. Has a left box, that will contain
 * the menu or live edit box. Also has a right box that will
 * change depending on what buttons were pressed.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class MainView extends Application {
  private HBox root;
  private VBox leftBox;
  private VBox rightBox;
  private MainViewController mainViewController;

  /**
   * Method that launches the application view.
   *
   * @param args array of strings that contains the command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Get-method to retrieve the content of the box on the
   * left of the application.
   *
   * @return left box
   */
  public VBox getLeftBox() {
    return leftBox;
  }

  /**
   * Get-method to retrieve the content of the box on the
   * right of the application.
   *
   * @return right box
   */
  public VBox getRightBox() {
    return rightBox;
  }

  /**
   * Get-method to retrieve the box that contains both the
   * left box and the right box.
   *
   * @return application view box
   */
  public HBox getRoot() {
    return root;
  }

  /**
   * Method that initializes the view contents set in
   * the init method in Application, as well as the
   * current initializations.
   *
   * @throws Exception if an error occurs from the JavaFX Application class
   */
  @Override
  public void init() throws Exception {
    super.init();
    leftBox = new VBox();
    rightBox = new VBox();
    mainViewController = new MainViewController(this);
  }

  /**
   * Method that stops the application. Uses the contents
   * of the stop method from Application.
   *
   * @throws Exception if an error occurs from the JavaFX Application class
   */
  @Override
  public void stop() throws Exception {
    super.stop();
  }

  /**
   * Method that starts the application and creates
   * the window that the application is displayed in.
   * ChatGPT helped with solving a problem related to resizing the fractal, by recommending
   * HBox.setHgrow, which was implemented.
   *
   * @param primaryStage main stage for the application
   */
  @Override
  public void start(Stage primaryStage) {
    root = new HBox();
    root.getChildren().addAll(leftBox, rightBox);

    Scene scene = new Scene(root, 1000, 700);
    scene.getStylesheets().add("style.css");

    leftBox.getStyleClass().add("leftBox");
    rightBox.getStyleClass().add("rightBox");

    leftBox.setMinWidth(200);
    leftBox.setMaxWidth(200);
    leftBox.setAlignment(Pos.TOP_CENTER);

    HBox.setHgrow(rightBox, Priority.ALWAYS);

    primaryStage.setTitle("Chaos game");
    primaryStage.setScene(scene);

    primaryStage.setMinWidth(750);
    primaryStage.setMinHeight(500);

    primaryStage.show();
  }

  /**
   * Method that creates and shows the alert to the user,
   * based on the given parameters.
   *
   * @param message the message to the user
   * @param alertType the type of alert
   */
  public void showAlert(String message, Alert.AlertType alertType) {
    Alert alert = new Alert(alertType);
    alert.setTitle(alertType.name());
    alert.setContentText(message);
    alert.showAndWait();
  }
}
