package edu.ntnu.idatt2003.view.uiBoxes;


import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Class that creates a box to be on the left side, that contains
 * the boxes and textfields necessary to be able to live edit all
 * relevant fields of the fractal.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class EditLiveFractalBox {
  //The goal was to have the width relative to the box, but we were not able to find another
  // solution that worked as expected
  private final int FULL_WIDTH = 200;
  private final int FULL_WIDTH_TXTFLD = 160;
  private final int HALF_WIDTH_TXTFLD = FULL_WIDTH_TXTFLD / 2;
  private final VBox editBox;
  private final VBox inputBox;
  private final ScrollPane scrollPane;
  private final Button btnBackToDashboard;
  private final Button btnSaveSetToFile;
  private final Text errorMessageForUser;
  private final TextField txtFldSteps;
  private List<TextField> listTxtFldCoords;
  private List<TextField> listTxtFldJulia;
  private List<List<TextField>> listTxtFldAffine;

  /**
   * Method that creates the boxes and buttons for
   * the live edit box, as well as to go back to the menu
   * or to save the current fractal.
   */
  public EditLiveFractalBox() {
    editBox = new VBox();
    inputBox = new VBox();
    scrollPane = new ScrollPane();
    btnBackToDashboard = new Button("Back");
    btnSaveSetToFile = new Button("Save fractal to file");
    errorMessageForUser = new Text("");

    txtFldSteps = new TextField();

    init();
  }

  /**
   * Get-method to retrieve the box containing all
   * elements for the live editing of the fields
   * for the fractals.
   *
   * @return box with all live edit elements
   */
  public VBox getEditBox() {
    return editBox;
  }

  /**
   * Get-method to retrieve the input box containing
   * all input textfields.
   *
   * @return input textfields box
   */
  public VBox getInputBox() {
    return inputBox;
  }

  /**
   * Get-method to retrieve the button to go back
   * to the dashboard.
   *
   * @return back to dashboard button
   */
  public Button getBtnBackToDashboard() {
    return btnBackToDashboard;
  }

  /**
   * Get-method to retrieve the button to save
   * the current fractal parameter set to a file.
   *
   * @return save parameter set to file button
   */
  public Button getBtnSaveSetToFile() {
    return btnSaveSetToFile;
  }

  /**
   * Get-method to access the textfield containing the
   * amount of steps.
   *
   * @return textfield for step amount
   */
  public TextField getTxtFldSteps() {
    return txtFldSteps;
  }

  /**
   * Get-method to access the list containing the values for the
   * textfields that contain both the minimum and the maximum
   * coordinates.
   *
   * @return list of min and max coord textfields
   */
  public List<TextField> getListTxtFldCoords() {
    return listTxtFldCoords;
  }

  /**
   * Get-method to access the list containing lists of
   * textfields for the affine2d transformations.
   *
   * @return list of lists of affine2d transform textfields
   */
  public List<List<TextField>> getListTxtFldAffine() {
    return listTxtFldAffine;
  }

  /**
   * Get-method to access the list of textfields containing
   * the values for the julia transform.
   *
   * @return list of julia transform textfields
   */
  public List<TextField> getListTxtFldJulia() {
    return listTxtFldJulia;
  }

  /**
   * Method that sets an error message for the user, based on a
   * given error message parameter.
   *
   * @param errorMessage error message to be displayed
   */
  public void setErrorMessageForUser(String errorMessage) {
    this.errorMessageForUser.setText(errorMessage);
  }

  /**
   * Method that initializes the live edit fractal box,
   * including showing all buttons, textfields,
   * error message, etc.
   */
  private void init() {
    scrollPane.setContent(inputBox);
    editBox.getChildren().addAll(btnBackToDashboard, btnSaveSetToFile, errorMessageForUser, scrollPane);
    editBox.setAlignment(Pos.TOP_CENTER);

    editBox.setMinWidth(FULL_WIDTH);
    editBox.setMaxWidth(FULL_WIDTH);

    editBox.getStyleClass().add("editBox");

    btnBackToDashboard.getStyleClass().add("editBoxButton");
    btnSaveSetToFile.getStyleClass().add("editBoxButton");
    btnBackToDashboard.setPrefWidth(FULL_WIDTH);
    btnSaveSetToFile.setPrefWidth(FULL_WIDTH);

    //Setting the wrapping width to follow FULL_WIDTH_TXTFLD to improve the visuals
    errorMessageForUser.setWrappingWidth(FULL_WIDTH_TXTFLD);
    errorMessageForUser.getStyleClass().add("errorMessage");
  }

  /**
   * Method that adds the textfield input for the amount of steps.
   */
  public void addStepsInput() {
    Text txtSteps = new Text("Amount of steps: ");

    inputBox.getChildren().addAll(txtSteps, txtFldSteps);
  }

  /**
   * Method that adds the input fields for the minimum and
   * maximum coordinate textfields.
   */
  public void addCoordsInput() {
    listTxtFldCoords = new ArrayList<>();

    Text txtMinCoords = new Text("Min coords:");

    TextField txtFldMinCoordsX0 = new TextField();
    TextField txtFldMinCoordsX1 = new TextField();

    Text txtMaxCoords = new Text("Max coords:");

    TextField txtFldMaxCoordsX0 = new TextField();
    TextField txtFldMaxCoordsX1 = new TextField();

    txtFldMinCoordsX0.setPrefWidth(FULL_WIDTH_TXTFLD);
    txtFldMinCoordsX1.setPrefWidth(FULL_WIDTH_TXTFLD);
    txtFldMaxCoordsX0.setPrefWidth(FULL_WIDTH_TXTFLD);
    txtFldMaxCoordsX1.setPrefWidth(FULL_WIDTH_TXTFLD);

    inputBox.getChildren().addAll(txtMinCoords, txtFldMinCoordsX0, txtFldMinCoordsX1, txtMaxCoords,
        txtFldMaxCoordsX0, txtFldMaxCoordsX1);

    listTxtFldCoords.add(txtFldMinCoordsX0);
    listTxtFldCoords.add(txtFldMinCoordsX1);
    listTxtFldCoords.add(txtFldMaxCoordsX0);
    listTxtFldCoords.add(txtFldMaxCoordsX1);
  }

  /**
   * Method that adds the fields for input for
   * julia transform texfields.
   */
  public void addJuliaInput() {
    listTxtFldJulia = new ArrayList<>();

    Text complexInputTxt = new Text("C-value \n(real part, imaginary part):");
    HBox complexHBox = new HBox();

    TextField realValueInput = new TextField();
    TextField imaginaryValueInput = new TextField();
    realValueInput.setPrefWidth(HALF_WIDTH_TXTFLD);
    imaginaryValueInput.setPrefWidth(HALF_WIDTH_TXTFLD);

    complexHBox.getChildren().addAll(realValueInput, imaginaryValueInput);
    inputBox.getChildren().addAll(complexInputTxt, complexHBox);

    listTxtFldJulia.add(realValueInput);
    listTxtFldJulia.add(imaginaryValueInput);

  }

  /**
   * Method that adds the fields for input for
   * affine transform texfields.
   *
   * @param affine2dInputIteration iteration for the current transform
   */
  public void addAffineInput(int affine2dInputIteration) {
    List<TextField> currentTransformationTextFields = new ArrayList<>();

    String affine2dInputIterationString = "Transformation ";
    String iterationString = affine2dInputIterationString + affine2dInputIteration + " :";
    Text iterationText = new Text(iterationString);

    Text matrixInputTxt = new Text("Matrix:");
    HBox matrixRow1 = new HBox();
    HBox matrixRow2 = new HBox();

    TextField a00Value = new TextField();
    TextField a01Value = new TextField();
    a00Value.setPrefWidth(HALF_WIDTH_TXTFLD);
    a01Value.setPrefWidth(HALF_WIDTH_TXTFLD);
    matrixRow1.getChildren().addAll(a00Value, a01Value);

    TextField a10Value = new TextField();
    TextField a11Value = new TextField();
    a10Value.setPrefWidth(HALF_WIDTH_TXTFLD);
    a11Value.setPrefWidth(HALF_WIDTH_TXTFLD);
    matrixRow2.getChildren().addAll(a10Value, a11Value);

    Text vectorInputTxt = new Text("Vector:");
    TextField b0Value = new TextField();
    TextField b1Value = new TextField();
    b0Value.setPrefWidth(FULL_WIDTH_TXTFLD);
    b1Value.setPrefWidth(FULL_WIDTH_TXTFLD);

    currentTransformationTextFields.add(a00Value);
    currentTransformationTextFields.add(a01Value);
    currentTransformationTextFields.add(a10Value);
    currentTransformationTextFields.add(a11Value);
    currentTransformationTextFields.add(b0Value);
    currentTransformationTextFields.add(b1Value);

    inputBox.getChildren().addAll(iterationText, matrixInputTxt, matrixRow1, matrixRow2, vectorInputTxt,
        b0Value, b1Value);

    listTxtFldAffine.add(currentTransformationTextFields);
  }

  /**
   * Method that clears the list of lists of
   * textfields containing the affine transform
   * fields.
   */
  public void clearListTxtFldAffine() {
    listTxtFldAffine = new ArrayList<>();
  }
}
