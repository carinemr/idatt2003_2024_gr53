package edu.ntnu.idatt2003.view.uiBoxes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Class that creates the dashboard box, that contains
 * methods to run transformations from the factory, or
 * to upload a file that fits the expected design of
 * the .txt file.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class DashboardBox {
  private final int BTN_RUN_WIDTH = 15;
  private final VBox dashboardBox;
  private final Text title;
  private final VBox descriptionBox;
  private final Button btnSierpinski;
  private final Button btnBarnsleyFern;
  private final Button btnJulia;
  private final Button btnUploadFile;

  /**
   * Constructor that creates the boxes and buttons to be
   * displayed on the dashboard.
   *
   * @throws FileNotFoundException if arrow button to run is not accessible
   */
  public DashboardBox() throws FileNotFoundException {
    dashboardBox = new VBox();

    title = new Text("Dashboard");
    descriptionBox = new VBox();

    btnSierpinski = new Button();
    btnBarnsleyFern = new Button();
    btnJulia = new Button();
    btnUploadFile = new Button("Upload file");

    init();
  }

  /**
   * Get-method to access the dashboard box, that
   * contains all elements on the dashboard page.
   *
   * @return dashboard box
   */
  public VBox getDashboardBox() {
    return dashboardBox;
  }

  /**
   * Get-method to access the button to run the
   * sierpinski fractal from the factory.
   *
   * @return run sierpinski fractal button
   */
  public Button getBtnSierpinski() {
    return btnSierpinski;
  }

  /**
   * Get-method to access the button to run the
   * barnsley fern fractal from the factory.
   *
   * @return run barnsley fern fractal button
   */
  public Button getBtnBarnsleyFern() {
    return btnBarnsleyFern;
  }

  /**
   * Get-method to access the button to run the
   * julia fractal from the factory.
   *
   * @return run julia fractal button
   */
  public Button getBtnJulia() {
    return btnJulia;
  }

  /**
   * Get-method to access the button to upload
   * a file that will be read and run.
   *
   * @return run uploaded file fractal button
   */
  public Button getBtnUploadFile() {
    return btnUploadFile;
  }

  /**
   * Method that initializes the content of the dashboard
   * box, including the table containing the buttons to
   * run factory fractals, or upload a separate file.
   *
   * @throws FileNotFoundException if the arrow image is not accessible
   */
  private void init() throws FileNotFoundException {
    Text txtGeneralInformation = new Text("Below you may view some standards fractals, as well "
        + "as any extra you may have added, and run them.\n");
    GridPane fractalTable = new GridPane();

    Image runImg = new Image(new FileInputStream("src/main/resources/images/run.png"));
    ImageView runImgSierpinski = new ImageView(runImg);
    ImageView runImgBarnsleyFern = new ImageView(runImg);
    ImageView runImgJulia = new ImageView(runImg);

    btnSierpinski.setGraphic(runImgSierpinski);
    runImgSierpinski.setFitWidth(BTN_RUN_WIDTH);
    runImgSierpinski.setPreserveRatio(true);

    btnBarnsleyFern.setGraphic(runImgBarnsleyFern);
    runImgBarnsleyFern.setFitWidth(BTN_RUN_WIDTH);
    runImgBarnsleyFern.setPreserveRatio(true);


    btnJulia.setGraphic(runImgJulia);
    runImgJulia.setFitWidth(BTN_RUN_WIDTH);
    runImgJulia.setPreserveRatio(true);

    title.getStyleClass().add("title");

    fractalTable.addColumn(0, new Label("Name"));
    fractalTable.addColumn(1, new Label("Type"));
    fractalTable.addColumn(2, new Label("Run"));

    fractalTable.addRow(1, new Text("Sierpinski"), new Text("Affine2D"),
        btnSierpinski);
    fractalTable.addRow(2, new Text("Barnsley fern"), new Text("Affine2D"),
        btnBarnsleyFern);
    fractalTable.addRow(3, new Text("Julia"), new Text("Julia"),
        btnJulia);

    fractalTable.setVgap(10);
    fractalTable.setHgap(30);

    dashboardBox.getStyleClass().add("generalBox");
    descriptionBox.getStyleClass().add("generalBox");

    descriptionBox.getChildren().addAll(txtGeneralInformation, fractalTable, btnUploadFile);
    dashboardBox.getChildren().addAll(title, descriptionBox);
  }
}
