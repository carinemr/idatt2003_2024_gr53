package edu.ntnu.idatt2003.view.uiBoxes;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Class that creates a box filled with boxes and the information
 * to be displayed on the help page.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class HelpBox {
  private final VBox rightSideHelpBox;
  private final ScrollPane scrollPane;
  private final VBox helpBox;

  /**
   * Constructor that creates all boxes and scrollpane
   * to display the help-page information.
   */
  public HelpBox() {
    rightSideHelpBox = new VBox();
    scrollPane = new ScrollPane();
    helpBox = new VBox();
    init();
  }

  /**
   * Get-method to retrieve the VBox containing all elements
   * on the help page.
   *
   * @return box with all help page elements
   */
  public VBox getRightSideHelpBox() {
    return rightSideHelpBox;
  }

  /**
   * Method that initializes the content on the help page.
   */
  private void init() {
    rightSideHelpBox.getChildren().add(scrollPane);
    scrollPane.setContent(helpBox);

    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.getStyleClass().add("scrollpane");

    helpBox.setMinWidth(500);
    helpBox.setMaxWidth(500);
    helpBox.getStyleClass().add("generalBox");

    fillHelpPage();
  }

  /**
   * Method to fill the page with the title, sub-titles and
   * help-page information.
   */
  private void fillHelpPage() {
    Text title = new Text("Help");
    title.getStyleClass().add("title");

    Text introText = new Text("Having difficulties figuring out what to do? This page may help!");

    Text dashboardSubtitle = new Text("The dashboard");
    dashboardSubtitle.getStyleClass().add("subtitle");
    Text dashboardInfo = new Text("Here you may view a list over the fractals you have stored in " +
        "the application. To run any of these, simply press the “run”-button on column on the " +
        "far-right. Wanting to create a new one, or upload a fractal from a file? You may either " +
        "create one in the app, or upload a txt-file from your computer. If you’re uploading a " +
        "file, it has to follow this format on the different lines: \n" +
        " 1. “Affine2D”/”Julia” (depending on what type of fractal) \n" +
        " 2. The lower left coordinates, with the x and y value separated by “,”.\n" +
        " 3. The upper right coordinates, with the same format as above\n" +
        " 4. The next lines contains the different “transformations”. For Affine2D there needs to " +
        "be 6 values on each line separated by “,”, with the first four being used for the matrix, " +
        "and the last two for the associated vector. For Julia there needs to be 2 values, also " +
        "separated by ”,”, representing the complex number the transformations will be created " +
        "from.");

    Text createFractalSubtitle = new Text("Create new fractal");
    createFractalSubtitle.getStyleClass().add("subtitle");
    Text createFractalInfo = new Text("Here you may create a custom fractal. However, be aware " +
        " that the values you enter might not result in a fractal that is possible to create.");

    Text viewRecentSubtitle = new Text("View recent fractal");
    viewRecentSubtitle.getStyleClass().add("subtitle");
    Text viewRecentInfo = new Text("Here you may view the most recent fractal that has been " +
        "“run”. This option will be possible to use once a fractal has been visualized.");

    Text editFractalSubtitle = new Text("When viewing the fractal");
    editFractalSubtitle.getStyleClass().add("subtitle");
    Text editFractalInfo = new Text("When viewing the fractal you may: \n" +
        " - Edit the amount of steps to be run, indicating the amount of points to be calculated. The more steps, the better the fractal will be. \n" +
        " - Change the lower left and upper right coordinates. \n" +
        " - Run more steps\n" +
        " - Save the fractal");

    Text exitSubtitle = new Text("Exit");
    exitSubtitle.getStyleClass().add("subtitle");
    Text exitInfo = new Text("This will exit the application, ending your current session.");

    helpBox.getChildren().addAll(title, introText, dashboardSubtitle, dashboardInfo,
        createFractalSubtitle, createFractalInfo, viewRecentSubtitle, viewRecentInfo,
        editFractalSubtitle, editFractalInfo, exitSubtitle, exitInfo);

    helpBox.getChildren().forEach(node -> {
        if (node instanceof Text text) {
          text.setWrappingWidth(helpBox.getMinWidth());
        }
    });
  }
}
