package edu.ntnu.idatt2003.view.uiBoxes;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Class that creates a box filled with fields and information
 * necessary or helpful when creating a page for custom fractal
 * description creation.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class CustomFractalCreationBox {
  private final int WIDTH = 500;
  private final VBox rightSideCreationBox;
  private final ScrollPane fractalCreationScroll;
  private final VBox fractalCreationBox;
  private final Text title;
  private final Text errorMessage;
  private final VBox descriptionBox;
  private final VBox minMaxInputBox;
  private final VBox transformationInputBox;
  private final VBox affine2dIndividualInputBox;
  private final ToggleGroup transformationToggle;
  private final RadioButton btnAffine2D;
  private final RadioButton btnJulia;
  private final Button btnAddAnotherTransformation;
  private final Button btnShowFractal;
  private int affine2dInputIteration;
  private final List<TextField> minMaxTextFields;
  private final List<List<TextField>> allTransformationTextFields;
  private final List<TextField> complexValueTextFields;

  /**
   * Constructor that creates all necessary boxes, buttons and lists
   * for creating the page to create a new fractal description.
   */
  public CustomFractalCreationBox() {
    rightSideCreationBox = new VBox();
    fractalCreationScroll = new ScrollPane();
    fractalCreationBox = new VBox();
    title = new Text("Create fractal");
    errorMessage = new Text("");
    descriptionBox = new VBox();
    minMaxInputBox = new VBox();
    transformationInputBox = new VBox();
    affine2dIndividualInputBox = new VBox();
    transformationToggle = new ToggleGroup();
    btnAffine2D = new RadioButton("Affine2D");
    btnJulia = new RadioButton("Julia");
    btnAddAnotherTransformation = new Button("Add another transformation");
    btnShowFractal = new Button("Show fractal");
    affine2dInputIteration = 1;
    minMaxTextFields = new ArrayList<>();
    allTransformationTextFields = new ArrayList<>();
    complexValueTextFields = new ArrayList<>();
    init();
  }

  /**
   * Get-method to access the box that contains everything in this class.
   *
   * @return VBox with all elements for fractal creation
   */
  public VBox getRightSideCreationBox() {
    return rightSideCreationBox;
  }

  /**
   * Get-method to access the box inside the scrollbox, that contains all elements
   * for creation and input.
   *
   * @return VBox all elements for input and creation
   */
  public VBox getFractalCreationBox() {
    return fractalCreationBox;
  }

  /**
   * Get-method for the ToggleGroup that contains the RadioButtons
   * for affine2D or julia.
   *
   * @return ToggleGroup containing julia and affine2d buttons
   */
  public ToggleGroup getTransformationToggle() {
    return transformationToggle;
  }

  /**
   * Get-method to retrieve the button for affine2d transformations.
   *
   * @return affine2d buttons
   */
  public RadioButton getBtnAffine2D() {
    return btnAffine2D;
  }

  /**
   * Get-method to retrieve the button for julia transformations.
   *
   * @return julia buttons
   */
  public RadioButton getBtnJulia() {
    return btnJulia;
  }

  /**
   * Get-method for retrieving the button to add another transformation.
   *
   * @return button to add another transformation
   */
  public Button getBtnAddAnotherTransformation() {
    return btnAddAnotherTransformation;
  }

  /**
   * Get-method for retrieving the button to show the fractal created
   * by the input parameters on the page.
   *
   * @return button to show fractal
   */
  public Button getBtnShowFractal() {
    return btnShowFractal;
  }

  /**
   * Get-method to retrieve the list containing the textfields from the
   * minimum and maximum coordinate text fields.
   *
   * @return list of textfields containing min and max coords
   */
  public List<TextField> getMinMaxTextFields() {
    return minMaxTextFields;
  }

  /**
   * Get-method to access the list containing the lists of textfields,
   * that contain the values for all affine2d transforms.
   *
   * @return list of lists with textfields for affine2d
   */
  public List<List<TextField>> getAllTransformationTextFields() {
    return allTransformationTextFields;
  }

  /**
   * Get-method to access the list of textfields for the complex
   * values for julia transforms.
   *
   * @return list of textfields for complex values
   */
  public List<TextField> getComplexValueTextFields() {
    return complexValueTextFields;
  }

  /**
   * Set-method that sets a new error message.
   *
   * @param errorMessage new error message
   */
  public void setErrorMessage(String errorMessage) {
    this.errorMessage.setText(errorMessage);
  }

  /**
   * Method to initialize the box, by filling it up with the
   * buttons, textfields and other elements.
   */
  private void init() {
    fillDescriptionBox();
    fillMinMaxInputBox();
    fillAffine2DTransformationInputBox();
    rightSideCreationBox.getChildren().add(fractalCreationScroll);
    fractalCreationScroll.setContent(fractalCreationBox);
    fractalCreationBox.getChildren().addAll(title, descriptionBox, minMaxInputBox,
        transformationInputBox, btnShowFractal, errorMessage);

    btnAffine2D.setToggleGroup(transformationToggle);
    btnJulia.setToggleGroup(transformationToggle);
    btnAffine2D.setSelected(true);

    rightSideCreationBox.getStyleClass().add("generalBox");
    fractalCreationBox.getStyleClass().add("generalBox");

    title.getStyleClass().add("title");

    fractalCreationScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    fractalCreationScroll.getStyleClass().add("scrollpane");
    fractalCreationBox.setMinWidth(WIDTH);
    fractalCreationBox.setMaxWidth(WIDTH);

    errorMessage.getStyleClass().add("errorMessage");
  }

  /**
   * Method to fill the description box, that contains the informative text for the page.
   */
  private void fillDescriptionBox() {
    descriptionBox.getChildren().clear();
    Text txtGeneralInformation = new Text("The following fields below need to be filled out to " +
        "create a fractal. You may choose between two-dimensional affine transformations or Julia " +
        "transformations. If you need to add several transformations for your fractal, simply press" +
        " “add another”.\n");

    txtGeneralInformation.setWrappingWidth(WIDTH);
    descriptionBox.getChildren().add(txtGeneralInformation);
  }

  /**
   * Method to fill in the box with the minimum and maximum input
   * textfields, as well as buttons for affine2d and julia to
   * choose what type of fractal is to be created.
   */
  private void fillMinMaxInputBox() {
    minMaxInputBox.getChildren().clear();
    HBox transformationTypeBox = new HBox();
    VBox fractalValuesInputBox = new VBox();

    Text transformationChoice = new Text("Type of transformation: ");
    transformationTypeBox.getChildren().addAll(transformationChoice, btnAffine2D, btnJulia);

    Text minCoordsTxt = new Text("Fill out min-coords:");
    TextField inputMinValueX0 = new TextField();
    inputMinValueX0.setPromptText("x0");
    TextField inputMinValueX1 = new TextField();
    inputMinValueX1.setPromptText("x1");

    Text maxCoordsTxt = new Text("Fill out max-coords:");
    TextField inputMaxValueX0 = new TextField();
    inputMaxValueX0.setPromptText("x0");
    TextField inputMaxValueX1 = new TextField();
    inputMaxValueX1.setPromptText("x1");
    fractalValuesInputBox.getChildren().addAll(minCoordsTxt, inputMinValueX0, inputMinValueX1, maxCoordsTxt,
        inputMaxValueX0, inputMaxValueX1);

    minMaxInputBox.getChildren().addAll(transformationTypeBox, fractalValuesInputBox);

    minMaxTextFields.add(inputMinValueX0);
    minMaxTextFields.add(inputMinValueX1);
    minMaxTextFields.add(inputMaxValueX0);
    minMaxTextFields.add(inputMaxValueX1);
  }

  /**
   * Method to fill the transformation input box with the initial
   * textfields for affine2d transformations. Also numbers the
   * transformation based on the iteration, that is set to 1.
   */
  public void fillAffine2DTransformationInputBox() {
    transformationInputBox.getChildren().clear();
    affine2dIndividualInputBox.getChildren().clear();
    affine2dInputIteration = 1;

    createAffine2DInputTextFields();

    transformationInputBox.getChildren().addAll(affine2dIndividualInputBox,
        btnAddAnotherTransformation);
  }

  /**
   * Method to add text fields for each round of transforms,
   * with round numberiong of the transforms being printed as well.
   */
  public void createAffine2DInputTextFields() {
    List<TextField> currentTransformationTextFields = new ArrayList<>();

    String affine2dInputIterationString = "Transformation ";
    String iterationString = affine2dInputIterationString + affine2dInputIteration;
    Text iterationText = new Text(iterationString);

    Text matrixInputTxt = new Text("Fill in the matrix:");
    HBox matrixRow1 = new HBox();
    HBox matrixRow2 = new HBox();

    TextField a00Value = new TextField();
    a00Value.setPromptText("a00");
    TextField a01Value = new TextField();
    a01Value.setPromptText("a01");
    matrixRow1.getChildren().addAll(a00Value, a01Value);

    TextField a10Value = new TextField();
    a10Value.setPromptText("a10");
    TextField a11Value = new TextField();
    a11Value.setPromptText("a11");
    matrixRow2.getChildren().addAll(a10Value, a11Value);

    Text vectorInputTxt = new Text("Fill in the vector:");
    TextField b1Value = new TextField();
    b1Value.setPromptText("b1");
    TextField b2Value = new TextField();
    b2Value.setPromptText("b2");

    currentTransformationTextFields.add(a00Value);
    currentTransformationTextFields.add(a01Value);
    currentTransformationTextFields.add(a10Value);
    currentTransformationTextFields.add(a11Value);
    currentTransformationTextFields.add(b1Value);
    currentTransformationTextFields.add(b2Value);

    affine2dIndividualInputBox.getChildren().addAll(iterationText, matrixInputTxt, matrixRow1, matrixRow2, vectorInputTxt,
        b1Value, b2Value);

    allTransformationTextFields.add(currentTransformationTextFields);
  }

  /**
   * Method that increases the transform iteration by 1.
   */
  public void incrementTransformationIteration() {
    affine2dInputIteration++;
  }

  /**
   * Method that fills the transformation input box with
   * input textfields for the real and imaginary parts
   * of a complex value.
   */
  public void fillJuliaTransformationInputBox() {
    transformationInputBox.getChildren().clear();

    Text complexInputTxt = new Text("Fill out the complex number c:");

    HBox inputHBox = new HBox();

    TextField realValueInput = new TextField();
    realValueInput.setPromptText("Real part");

    TextField imaginaryValueInput = new TextField();
    imaginaryValueInput.setPromptText("Imaginary part");

    inputHBox.getChildren().addAll(realValueInput, imaginaryValueInput);

    transformationInputBox.getChildren().addAll(complexInputTxt, inputHBox);

    complexValueTextFields.add(realValueInput);
    complexValueTextFields.add(imaginaryValueInput);
  }
}
