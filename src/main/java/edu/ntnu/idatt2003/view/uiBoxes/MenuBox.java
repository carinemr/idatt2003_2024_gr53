package edu.ntnu.idatt2003.view.uiBoxes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * Class that creates a box that contains the menu box,
 * to navigate through the application.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class MenuBox {
  private final int MENUBOX_WIDTH = 200;
  private final VBox menuBox;
  private final Button btnDashboard;
  private final Button btnCreateNewFractal;
  private final Button btnViewRecentFractal;
  private final Button btnHelp;
  private final Button btnQuitApplication;

  /**
   * Creates the menu box and the buttons that the can be
   * used to navigate in the application.
   *
   * @throws FileNotFoundException if the application can't find the logo
   */
  public MenuBox() throws FileNotFoundException {
    menuBox = new VBox();
    menuBox.getStyleClass().add("menuBox");
    btnDashboard = new Button("Dashboard");
    btnCreateNewFractal = new Button("Create new fractal");
    btnViewRecentFractal = new Button("View recent fractal");
    btnHelp = new Button("Help");
    btnQuitApplication = new Button("Quit");
    createMenu();
  }

  /**
   * Get-method to retrieve the box with all menu items.
   *
   * @return VBox with menu elements
   */
  public VBox getMenuBox() {
    return menuBox;
  }

  /**
   * Get-method to access the 'dashboard' button.
   *
   * @return 'dashboard' button
   */
  public Button getBtnDashboard() {
    return btnDashboard;
  }

  /**
   * Get-method to access the 'create new fractal' button.
   *
   * @return 'create new fractal' button
   */
  public Button getBtnCreateNewFractal() {
    return btnCreateNewFractal;
  }

  /**
   * Get-method to access the 'view recent fractal' button.
   *
   * @return 'view recent fractal' button
   */
  public Button getBtnViewRecentFractal() {
    return btnViewRecentFractal;
  }

  /**
   * Get-method to access the 'help' button.
   *
   * @return 'help' button
   */
  public Button getBtnHelp() {
    return btnHelp;
  }

  /**
   * Get-method to access the 'quit' button.
   *
   * @return 'quit' button
   */
  public Button getBtnQuitApplication() {
    return btnQuitApplication;
  }

  /**
   * Method that displays the logo and creates the menu buttons.
   *
   * @throws FileNotFoundException if logo picture is not found
   */
  public void createMenu() throws FileNotFoundException {
    menuBox.getChildren().clear();
    Image logo = new Image(new FileInputStream("src/main/resources/images/logo.png"));
    ImageView logoView = new ImageView(logo);

    menuBox.setMinWidth(MENUBOX_WIDTH);
    menuBox.setMaxWidth(MENUBOX_WIDTH);
    menuBox.setAlignment(Pos.TOP_CENTER);

    //The value here is hard-coded due to not having enough time to figure out a different solution
    logoView.setFitWidth(MENUBOX_WIDTH);
    logoView.setPreserveRatio(true);

    List<Button> buttons = new ArrayList<>();
    buttons.add(btnDashboard);
    buttons.add(btnCreateNewFractal);
    buttons.add(btnHelp);
    buttons.add(btnQuitApplication);

    buttons.forEach(button -> {
      button.setPrefWidth(MENUBOX_WIDTH);
      button.getStyleClass().add("menuBoxButton");
    });
    btnViewRecentFractal.setPrefWidth(MENUBOX_WIDTH);
    btnViewRecentFractal.getStyleClass().add("inactiveButton");

    menuBox.getChildren().addAll(logoView, btnDashboard, btnCreateNewFractal, btnViewRecentFractal,
        btnHelp, btnQuitApplication);
  }
}
