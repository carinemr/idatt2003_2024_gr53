package edu.ntnu.idatt2003.view.uiBoxes;

import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * Class that creates the box to displau the fractal image.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class FractalDisplayBox {
  private final VBox fractalDisplayBox;
  private final ImageView fractalImage;

  /**
   * Constructor that creates the box and ImageView to
   * display the fractal.
   */
  public FractalDisplayBox() {
    fractalDisplayBox = new VBox();
    fractalImage = new ImageView();

    init();
  }

  /**
   * Get-method to access the fractal display box.
   *
   * @return box with fractal displayed
   */
  public VBox getFractalDisplayBox() {
    return fractalDisplayBox;
  }

  /**
   * Get-method to access the ImageView for the display
   * of the fractal.
   *
   * @return fractal ImageView
   */
  public ImageView getFractalImage() {
    return fractalImage;
  }

  /**
   * Method that initiates the fractal display box to
   * add the fractal image.
   */
  public void init() {
    fractalDisplayBox.getChildren().add(fractalImage);
  }
}
