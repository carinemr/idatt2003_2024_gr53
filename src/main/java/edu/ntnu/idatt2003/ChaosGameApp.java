package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.view.MainView;

/**
 * Class that is responsible for running the application.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 */
public class ChaosGameApp {
  /**
   * Method that launches the application.
   * @param args array of strings that contains the command line arguments
   */
  public static void main(String[] args) {
    MainView.main(args);
  }
}
