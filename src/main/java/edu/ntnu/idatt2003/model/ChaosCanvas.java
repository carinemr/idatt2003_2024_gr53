package edu.ntnu.idatt2003.model;

import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import java.util.Arrays;

/** Class that represents a canvas to show fractals. It has methods for clearing the canvas and
 * assigning a color, represented by the number 1, to pixels in the canvas.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 */
public class ChaosCanvas {
  private final int[][] canvas;
  private final int width;
  private final int height;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private AffineTransform2D transformCoordsToIndices;

  /** Constructor for the ChaosCanvas class. The constructor initializes a ChaosCanvas-object with
   * parameters for the width and height of the canvas, as well as the minimum and maximum coords
   * of the fractal to be drawn. It creates a two-dimensional array based on the height and width,
   * and sets all values to 0 by calling the clear-method.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @param minCoords the minimum coordinates for the fractal
   * @param maxCoords the maximum coordinates for the fractal
   * @see #clear()
   */
  public ChaosCanvas (int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    validateCanvasInput(width, height, minCoords, maxCoords);
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[height][width];
    this.transformCoordsToIndices =
        calculateTransformCoordToIndices(width, height, minCoords, maxCoords);
    clear();
  }

  /** Method to get the width of the canvas.
   *
   * @return width of canvas
   */
  public int getWidth() {
    return width;
  }

  /** Method to get height of the canvas.
   *
   * @return height of canvas
   */
  public int getHeight() {
    return height;
  }

  /** Method to get the minimum coordinates of the fractal related to the ChaosGame.
   *
   * @return minimum coordinates
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /** Method to get the maximum coordinates of the fractal related to the ChaosGame.
   *
   * @return maximum coordinates
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /** Method to get the two-dimensional canvas.
   *
   * @return an array representing the canvas
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /** Method to set the minimum coordinates. It firstly validates the parameter. After this it sets
   * the coords to the new coords, and updates the transformCoordsToIndices-field.
   *
   * @param newMinCoords the new minimum coordinates
   * @throws IllegalArgumentException if the parameter is null or greater than the maximum coords.
   */
  public void setMinCoords(Vector2D newMinCoords) throws IllegalArgumentException {
    if (newMinCoords == null) {
      throw new IllegalArgumentException("Could not set new min coords due to new values being null");
    }
    if (newMinCoords.getX0() >= maxCoords.getX0() || newMinCoords.getX1() >= maxCoords.getX1()) {
      throw new IllegalArgumentException("Could not set new min coords due to new x0 or x1 being greater than the " +
          "max coord counterpart");
    }
    this.minCoords = newMinCoords;
    this.transformCoordsToIndices = calculateTransformCoordToIndices(width, height, newMinCoords,
        maxCoords);
  }

  /** Method to set the maximum coordinates. It firstly validates the parameter. After this it sets
   * the coords to the new coords, and updates the transformCoordsToIndices-field.
   *
   * @param newMaxCoords the new minimum coordinates
   * @throws IllegalArgumentException if the parameter is null or less than the minimum coords.
   */
  public void setMaxCoords(Vector2D newMaxCoords) throws IllegalArgumentException {
    if (newMaxCoords == null) {
      throw new IllegalArgumentException("Could not set new min coords due to new values being null");
    }
    if (newMaxCoords.getX0() <= minCoords.getX0() || newMaxCoords.getX1() <= minCoords.getX1()) {
      throw new IllegalArgumentException("Could not set new max coords due to new x0 or x1 being lesser than the " +
          "min coord counterpart");
    }
    this.maxCoords = newMaxCoords;
    transformCoordsToIndices = calculateTransformCoordToIndices(width, height, minCoords,
        newMaxCoords);
  }

  /** Method for calculating an affine transformation that is used to translate a fractal-point to
   * a canvas-coordinate.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @param minCoords the minimum coordinates of the fractal
   * @param maxCoords the maximum coordinates of the fractal
   * @return an affine transformation
   */
  public AffineTransform2D calculateTransformCoordToIndices(
      int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    validateCanvasInput(width, height, minCoords, maxCoords);
    double a00 = 0, a11 = 0;
    double a01 = (height - 1)/(minCoords.getX1() - maxCoords.getX1());
    double a10 = (width - 1)/(maxCoords.getX0() - minCoords.getX0());

    double b0 = (height - 1) * maxCoords.getX1() / (maxCoords.getX1() - minCoords.getX1());
    double b1 = (width - 1) * minCoords.getX0() / (minCoords.getX0() - maxCoords.getX0());

    Matrix2x2 matrix2x2 = new Matrix2x2(a00, a01, a10, a11);
    Vector2D bVector = new Vector2D(b0, b1);

    return new AffineTransform2D(matrix2x2, bVector);
  }

  /** Method for getting the value of a specific element in the canvas. It firstly turns the fractal
   * point-parameter into a coordinate.
   *
   * @param point the fractal point
   * @return the value of the element
   */
  public int getPixel(Vector2D point) {
    Vector2D coordinates = transformCoordsToIndices.transform(point);
    validateCoordinate(coordinates);

    return canvas[(int) coordinates.getX0()][(int) coordinates.getX1()];
  }

  /** Method for increasing the value of an element in the canvas. It firstly turns the fractal
   * point-parameter into a coordinate, retrieves the value of the element, increases it and sets
   * the new value. It increases by 1.
   *
   * @param point the fractal point
   */
  public void putPixel(Vector2D point) {
    Vector2D coordinates = transformCoordsToIndices.transform(point);
    validateCoordinate(coordinates);

    int value = getPixel(point);
    value++;

    canvas[(int) coordinates.getX0()][(int) coordinates.getX1()] = value;
  }

  /** Method for validating the input for the constructor. It controls that the height and
   * width are not less than 0, and that the minimum coords are less than the maximum coords, as
   * well as not null.
   *
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @param minCoords the minimum coords of the fractal related to the ChaosGame
   * @param maxCoords the maximum coords of the fractal related to the ChaosGame
   * @throws IllegalArgumentException if the height or width are less than 1, the min- or maxcoords
   *                                  is null, or the mincoords is greater than the maxcoords
   */
  private void validateCanvasInput(int width, int height, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException{
    if (width < 1 || height < 1) {
      throw new IllegalArgumentException("Error: Cannot make a canvas with values less than 1.");
    }
    if(minCoords == null || maxCoords == null) {
      throw new IllegalArgumentException("Error: Coordinates may not be null");
    }
    if (minCoords.getX0() > maxCoords.getX0() || minCoords.getX1() > maxCoords.getX1()) {
      throw new IllegalArgumentException("Error: The values of maxCoords were not greater than" +
          " the values of minCoords.");
    }
  }

  /** Method for validating a coordinate for the canvas.
   *
   * @param vector2D the coordinate for the canvas
   * @throws IllegalArgumentException if the coordinate is outside the canvas parameters
   */
  private void validateCoordinate(Vector2D vector2D) throws IllegalArgumentException{
    if ((int) vector2D.getX0() < 0 || (int) vector2D.getX0() >= height) {
      throw new ArrayIndexOutOfBoundsException("The coordinate was found to be outside the canvas due to" +
          " exceeding the height or being less than 0.");
    } else if ((int) vector2D.getX1() < 0 || (int) vector2D.getX1() >= width) {
      throw new ArrayIndexOutOfBoundsException("The coordinate was found to be outside the canvas due to" +
          " exceeding the width or being less than 0.");
    }
  }



  /** Method for clearing the canvas by setting each element in all rows to zero, indicating
   * the color white.
   * This method was made by ChatGPT. The original plan was to use for-loop, but the
   * usage of streams was preferred to make the code more readable. By using streams
   * and foreach, this method runs through each row of the canvas, and sets all
   * elements to zero through Arrays' own fill-method.
   */
  public void clear() {
    Arrays.stream(canvas).forEach(row -> Arrays.fill(row, 0));
  }
}
