package edu.ntnu.idatt2003.model.factory;

import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a factory to create descriptions for three fractals with pre-determined
 * values; sierpinski, barnsley fern and julia fractals.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class ChaosGameDescriptionFactory {
  /**
   * Factory method that created a ChaosGameDescription based on the fractal name parameter.
   *
   * @param fractalName name of the fractal type that is to be created
   * @return the description for the given fractal
   * @throws IllegalArgumentException if the fractalName is something other than the three
   */
  public static ChaosGameDescription getDescription(String fractalName)
      throws IllegalArgumentException {
    return switch (fractalName.toLowerCase()) {
      case "sierpinski" -> getSierpinski();
      case "barnsley_fern" -> getBarnsleyFern();
      case "julia" -> getJulia();
      default -> throw new IllegalArgumentException("Unexpected value: " + fractalName);
    };
  }

  /**
   * Method that creates a sierpinski description with pre-determined values.
   *
   * @return sierpinski description
   */
  private static ChaosGameDescription getSierpinski() {
    Matrix2x2 matrix1 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector1 = new Vector2D(0, 0);
    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);

    Matrix2x2 matrix2 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector2 = new Vector2D(.25, .5);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);

    Matrix2x2 matrix3 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector3 = new Vector2D(.5, 0);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);

    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);

    List<Transform2D> transformList = new ArrayList<>();
    transformList.add(transform1);
    transformList.add(transform2);
    transformList.add(transform3);

    return new ChaosGameDescription(transformList, minCoords, maxCoords);
  }

  /**
   * Method that creates a barnsley fern description with pre-determined values.
   *
   * @return barnsley fern description
   */
  private static ChaosGameDescription getBarnsleyFern() {
    Matrix2x2 matrix1 = new Matrix2x2(0, 0, 0, .16);
    Vector2D vector1 = new Vector2D(0, 0);

    Matrix2x2 matrix2 = new Matrix2x2(.85, .04, -.04, .85);
    Vector2D vector2 = new Vector2D(0, 1.6);

    Matrix2x2 matrix3 = new Matrix2x2(.2, -.26, .23, .22);
    Vector2D vector3 = new Vector2D(0, 1.6);

    Matrix2x2 matrix4 = new Matrix2x2(-.15, .28, .26, .24);
    Vector2D vector4 = new Vector2D(0, .44);

    Vector2D minCoords = new Vector2D(-2.65, 0);
    Vector2D maxCoords = new Vector2D(2.65, 10);

    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);
    AffineTransform2D transform4 = new AffineTransform2D(matrix4, vector4);

    List<Transform2D> transformList = new ArrayList<>();
    transformList.add(transform1);
    transformList.add(transform2);
    transformList.add(transform3);
    transformList.add(transform4);

    return new ChaosGameDescription(transformList, minCoords, maxCoords);
  }

  /**
   * Method that creates a julia description with pre-determined values.
   *
   * @return julia description
   */
  private static ChaosGameDescription getJulia() {
    int[] signArray = {-1, 1};
    List<Transform2D> transformList = new ArrayList<>();

    Complex c = new Complex(-.74543, .11301);
    Vector2D minCoords = new Vector2D(-1.6, -1);
    Vector2D maxCoords = new Vector2D(1.6, 1);

    for (int value : signArray) {
      transformList.add(new JuliaTransform(c, value));
    }

    return new ChaosGameDescription(transformList, minCoords, maxCoords);
  }
}
