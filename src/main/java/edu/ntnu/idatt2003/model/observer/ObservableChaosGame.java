package edu.ntnu.idatt2003.model.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class that contains a list of observers, the ability to add or
 * remove them from the list, as well as notifying observers of changes
 * on the canvas or description.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public abstract class ObservableChaosGame {
  private final List<ChaosGameObserver> observers;

  /**
   * Constructor that initiates the list to be filled with observers.
   */
  public ObservableChaosGame() {
    observers = new ArrayList<>();
  }

  /**
   * Method to add an observer to the list.
   *
   * @param observer the observer to be added
   */
  public void addObserver(ChaosGameObserver observer) {
    observers.add(observer);
  }

  /**
   * Method to remove an observer from the list.
   *
   * @param observer the observer to be removed
   */
  public void removeObserver(ChaosGameObserver observer) {
    observers.remove(observer);
  }

  /**
   * Method to notify the observers of changes in the description.
   */
  public void notifyObserversOfDescriptionChange() {
    observers.forEach(ChaosGameObserver::descriptionUpdate);
  }

  /**
   * Method to notify the observers of changes on the canvas.
   */
  public void notifyObserversOfUpdatedCanvas() {
    observers.forEach(ChaosGameObserver::canvasUpdate);
  }
}
