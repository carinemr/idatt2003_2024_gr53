package edu.ntnu.idatt2003.model.observer;

/**
 * Interface for the observer for the chaos game.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 */
public interface ChaosGameObserver {
  /**
   * Interface method for updating the canvas.
   */
  void canvasUpdate();

  /**
   * Interface method for updating the description.
   */
  void descriptionUpdate();

}
