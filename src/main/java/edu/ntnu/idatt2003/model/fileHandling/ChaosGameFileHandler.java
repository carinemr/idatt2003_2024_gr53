package edu.ntnu.idatt2003.model.fileHandling;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

/** Class that handles files related to ChaosGames. It contains methods for reading from, and
 * writing to, files. It also contains methods for parsing the read file.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 */
public class ChaosGameFileHandler {
  private static final String DELIMITER = ", ";
  private static final int[] signArray = {-1, 1};

  /** <p> Method for creating a description from a file, with the file's path as the parameter. It
   * uses a scanner to read the text in the file. It registers the type of transformation (Julia
   * or Affine2D), the min- and maxcoords, and the transformations given in the file. If an error
   * occurs when parsing, exceptions are caught and thrown.
   * </p>
   * <p>
   * ChatGPT helped with and came with recommendations for how to use the scanner. It also came
   * with tips on how to split the lines of the file. Using the scanner as a parameter inside
   * the try ensures that it is automatically closed when done running or if an exception happens.
   * The pattern for the delimiter ensures that any line break is used as a delimiter.
   * </p>
   *
   * @param path the path of the file
   * @return a description based on the read file
   * @throws IOException if there is an issue with the file
   * @throws IllegalArgumentException if there is an issue when parsing the read file
   */
  public static ChaosGameDescription readFromFile(String path)
      throws IOException, IllegalArgumentException{
    ChaosGameDescription description;
    Vector2D minCoords;
    Vector2D maxCoords;
    List<Transform2D> transforms = new ArrayList<>();

    try (Scanner sc = new Scanner(new File(path))) {

      sc.useDelimiter("\\R");

      String type = sc.next();
      type = cleanString(type);
      minCoords = parseVector(sc.next());
      maxCoords = parseVector(sc.next());

      if (type.equals("Affine2D")) {
        while (sc.hasNext()) {
          transforms.add(parseAffine2DTransform(sc.next()));
        }
      } else if (type.equals("Julia")) {
        String cValue = sc.next();
        transforms.add(parseJuliaTransform(cValue, signArray[0]));
        transforms.add(parseJuliaTransform(cValue, signArray[1]));
      } else {
        throw new IllegalArgumentException("Invalid transformation line, the first line should say Affine2D or Julia");
      }

    } catch (IOException e) {
      throw new IOException("Unable to read data, given input: " + path);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException(e.getMessage());
    }

    description = new ChaosGameDescription(transforms, minCoords, maxCoords);

    return description;
  }

  /** Method for removing any comments from a string. This removes everything after a possible # of
   * the string, which indicates a comment.
   *
   * @param string a line from a txt-file to be cleaned of any comments
   * @return a string without comments
   * @throws IllegalArgumentException if the string is empty
   */
  public static String cleanString(String string) throws IllegalArgumentException{
    if (string.isEmpty()) {
      throw new IllegalArgumentException("Cannot clean string, as the string is empty");
    }
    int indexHashtag = string.indexOf("#");
    if (indexHashtag != -1) {
      return string.substring(0, indexHashtag).trim();
    }
    return string.trim();
  }

  /** Method for parsing a vector from a string.
   *
   * @param line a line to create a vector from
   * @return a vector
   * @throws IllegalArgumentException if the string is empty, or an error occurs when trying to
   *                                  parse
   * @throws NoSuchElementException if there are an invalid amount of values
   */
  public static Vector2D parseVector(String line) throws IllegalArgumentException,
      NoSuchElementException {
    if (line.isEmpty()) {
      throw new IllegalArgumentException("Cannot parse vector, as the line is empty");
    }
    double x0;
    double x1;
    line = cleanString(line);
    Scanner values = new Scanner(line).useDelimiter(DELIMITER);
    values.useLocale(Locale.US);

    try {
      x0 = values.nextDouble();
      x1 = values.nextDouble();

      if (values.hasNext()) {
        throw new IllegalArgumentException("Invalid amount of values, expected 2 double values, " +
            "but got more.");
      }
    } catch (NoSuchElementException e){
      throw new NoSuchElementException("Invalid amount of values, expected 2 double values, " +
          "but got less or no doubles.");
    } catch (Exception e) {
      throw new IllegalArgumentException("Could not parse values to double.");
    }

    return new Vector2D(x0, x1);
  }

  /** Method for parsing an affine transformation from a string.
   *
   * @param line the line to create an affine transformation from
   * @return an affine transformation
   * @throws IllegalArgumentException if the line is empty, or an error occurs when parsing
   * @throws NoSuchElementException if there are an invalid amount of values
   */
  public static Transform2D parseAffine2DTransform(String line) throws IllegalArgumentException,
      NoSuchElementException {
    if (line.isEmpty()) {
      throw new IllegalArgumentException("Cannot parse vector, as the line is empty");
    }
    double b0, b1;
    double a00, a01, a10, a11;
    Matrix2x2 a;
    Vector2D b;
    line = cleanString(line);

    Scanner values = new Scanner(line).useDelimiter(DELIMITER);
    values.useLocale(Locale.US);

    try {
      a00 = values.nextDouble();
      a01 = values.nextDouble();
      a10 = values.nextDouble();
      a11 = values.nextDouble();
      b0 = values.nextDouble();
      b1 = values.nextDouble();

      if (values.hasNext()) {
        throw new IllegalArgumentException("Invalid amount of values, expected 6 values, but " +
            "got more.");
      }
    } catch (NoSuchElementException e){
      throw new NoSuchElementException("Invalid amount of values, expected 6 double values, " +
          "but got less or no doubles.");
    } catch (Exception e) {
      throw new IllegalArgumentException("Could not parse values to double.");
    }

    a = new Matrix2x2(a00, a01, a10, a11);
    b = new Vector2D(b0, b1);

    return new AffineTransform2D(a, b);
  }

  /** Method for parsing a Julia transformation from a string.
   *
   * @param line the line to create a Julia transformation from
   * @return a Julia transformation
   * @throws IllegalArgumentException if the line is empty, or an error occurs when parsing
   */
  public static Transform2D parseJuliaTransform(String line, int sign)
      throws IllegalArgumentException {
    if (line.isEmpty()) {
      throw new IllegalArgumentException("Cannot parse vector, as the line is empty");
    }
    line = cleanString(line);
    Vector2D vector = parseVector(line);
    Complex c = new Complex(vector.getX0(), vector.getX1());

    return new JuliaTransform(c, sign);
  }

  /** Method for writing a description to a file, based on a given path. By using the FileWriter
   * in the parentheses of the try-statement, the file will automatically be closed after the
   * code in the try-statement has been executed.
   *
   * @param description the description to write to the file
   * @param path the path for the file to be put
   * @throws IllegalArgumentException if the description is null, or if an error occurs when trying
   *                                  to write
   */
  public static void writeToFile(ChaosGameDescription description, String path)
      throws IllegalArgumentException {
    if (description == null) {
      throw new IllegalArgumentException("Cannot write to file due to null-value of description");
    }
    try (FileWriter fileWriter = new FileWriter(path)) {
      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

      List<Transform2D> listOfTransform = description.getTransforms();
      String transformationType;
      StringBuilder stringTransformations = new StringBuilder();

      if (listOfTransform.stream().
          allMatch((Transform2D transform) -> transform instanceof JuliaTransform)) {
        transformationType = "Julia";
        for (Transform2D transform2D : listOfTransform) {
          Complex c = ((JuliaTransform) transform2D).getPoint();
          stringTransformations.append(c.getX0()).append(", ").append(c.getX1()).append("\n");
        }
      } else if (listOfTransform.stream().
          allMatch((Transform2D transform) -> transform instanceof AffineTransform2D)) {
        transformationType = "Affine2D";
        for (Transform2D transform : listOfTransform) {
          Matrix2x2 a = ((AffineTransform2D) transform).getMatrix();
          Vector2D b = ((AffineTransform2D) transform).getVector();

          stringTransformations.append(a.getA00()).append(", ");
          stringTransformations.append(a.getA01()).append(", ");
          stringTransformations.append(a.getA10()).append(", ");
          stringTransformations.append(a.getA11()).append(", ");
          stringTransformations.append(b.getX0()).append(", ");
          stringTransformations.append(b.getX1()).append("\n");
        }
      } else {
        throw new IllegalArgumentException("Invalid type of transform list.");
      }

      Vector2D minCoords = description.getMinCoords();
      String stringMinCoords = minCoords.getX0() + ", " + minCoords.getX1();

      Vector2D maxCoords = description.getMaxCoords();
      String stringMaxCoords = maxCoords.getX0() + ", " + maxCoords.getX1();

      bufferedWriter.write(transformationType + "\n");
      bufferedWriter.write(stringMinCoords + "\n");
      bufferedWriter.write(stringMaxCoords + "\n");
      bufferedWriter.write(stringTransformations.toString());

      bufferedWriter.flush();

    } catch (IOException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }
}
