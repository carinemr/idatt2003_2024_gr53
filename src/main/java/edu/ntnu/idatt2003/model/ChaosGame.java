package edu.ntnu.idatt2003.model;

import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import edu.ntnu.idatt2003.model.observer.ObservableChaosGame;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import java.util.List;
import java.util.Random;

/** Class that represents a Chaos Game. It creates a canvas from a given description, and
 * communicates with both the canvas and description to do necessary changes. It also extends
 * ObservableChaosGame, and calls on the relevant notification-methods when relevant.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class ChaosGame extends ObservableChaosGame {
  private final ChaosCanvas canvas;
  private final ChaosGameDescription description;
  private Vector2D currentPoint;
  public Random random = new Random();

  /** Constructor for the ChaosGame-class. It initializes a ChaosGame-object with given parameters
   * for width and height for the canvas, as well as a description. It also initializes a
   * ChaosCanvas-object, that is based on the description, width and height-parameters.
   *
   * @param description the description of the fractal
   * @param width the width of the canvas
   * @param height the height of the canvas
   * @throws IllegalArgumentException if the description is null, or the height and width
   *                                  is less than 0
   */
  public ChaosGame(ChaosGameDescription description, int width,
                   int height) throws IllegalArgumentException{
    if (description == null) {
      throw new IllegalArgumentException("Cannot create ChaosGame as description is null");
    }
    if (width < 1 || height < 1) {
      throw new IllegalArgumentException("Cannot create ChaosGame as width or height is less "
          + "than 1");
    }

    this.canvas = new ChaosCanvas(width, height, description.getMinCoords(), description.getMaxCoords());
    this.description = description;
    this.currentPoint = new Vector2D(0, 0);
  }

  /** Method to get the ChaosCanvas-object.
   *
   * @return the ChaosCanvas-object
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /** Method to get the ChaosGameDescription.
   *
   * @return the ChaosGameDescription-object.
   */
  public ChaosGameDescription getDescription() {
    return description;
  }

  /** Method to get the current point on the fractal.
   *
   * @return the current point on the fractal.
   */
  public Vector2D getCurrentPoint() {
    return currentPoint;
  }

  /** Method to set all transformations of the description. It calls on the setTransform-method
   * in the ChaosGameDescription-class. After setting the transforms, the observers are notified.
   *
   * @param newTransforms new list of transformation
   */
  public void setTransforms(List<Transform2D> newTransforms) {
    description.setTransforms(newTransforms);

    notifyObserversOfDescriptionChange();
  }

  /** Method to set a specific transformations in the description. It calls on the
   * setSpecificTransform-method in the ChaosGameDescription-class. After setting the specific
   * transformation, the observers are notified.
   *
   * @param index the index of the transformation to be changed
   * @param newTransform the new transformation
   */
  public void setSpecificTransform(int index, Transform2D newTransform) {
    description.setSpecificTransform(index, newTransform);

    notifyObserversOfDescriptionChange();
  }

  /** Method to set the minimum coordinates of the fractal. It sets the mincoords in both the
   * description-object, and the canvas-object. After this, the observers are notified.
   *
   * @param newMinCoords the new mincoords
   */
  public void setMinCoords(Vector2D newMinCoords) {
    description.setMinCoords(newMinCoords);
    canvas.setMinCoords(newMinCoords);

    notifyObserversOfDescriptionChange();
  }

  /** Method to set the maximum coordinates of the fractal. It sets the maxcoords in both the
   * description-object, and the canvas-object. After this, the observers are notified.
   *
   * @param newMaxCoords the new maxcoords
   */
  public void setMaxCoords(Vector2D newMaxCoords) {
    description.setMaxCoords(newMaxCoords);
    canvas.setMaxCoords(newMaxCoords);

    notifyObserversOfDescriptionChange();
  }

  /** Method to put points on the canvas to visualize the fractal. The canvas is cleared, then
   * the currentPoint is set to be in the middle of the canvas. After this, for each step, a new
   * point is put based on a random transformation from the transformation-list of the description.
   * Lastly, the observers are notified of a canvas-update.
   *
   * @param steps the amount of steps, and points, to be drawn
   * @throws IllegalArgumentException if the steps are less than 0
   */
  public void runSteps(int steps) throws IllegalArgumentException{
    if (steps < 0) {
      throw new IllegalArgumentException("The amount of steps should be 1 or higher.");
    }

    canvas.clear();

    double x0 = (description.getMinCoords().getX0() + description.getMaxCoords().getX0())/2;
    double x1 = (description.getMinCoords().getX1() + description.getMaxCoords().getX1())/2;

    currentPoint = new Vector2D(x0,x1);

    for (int i = 0; i < steps; i++) {
      int dice = random.nextInt(description.getTransforms().size());
      Transform2D transformation = description.getTransforms().get(dice);
      currentPoint = transformation.transform(currentPoint);
      canvas.putPixel(currentPoint);
    }

    notifyObserversOfUpdatedCanvas();
  }
}
