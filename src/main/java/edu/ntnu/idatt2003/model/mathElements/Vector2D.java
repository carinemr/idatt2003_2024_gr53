package edu.ntnu.idatt2003.model.mathElements;

/**
 * Class that represents a two-dimensional vector. Also has methods to add and subtract another vector,
 * as well as a method that checks if a vector is null.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 * @see #Vector2D(double, double)
 * @see #add(Vector2D)
 * @see #subtract(Vector2D)
 * @see #checkVector(Vector2D)
 */
public class Vector2D {
  private final double x0;
  private final double x1;

  /**
   * Constructor for the Vector2D class. Initializes a vector with 2 parameters representing the two elements of
   * the vector.
   *
   * @param x0 represents the first element of the vector
   * @param x1 represents the second element of the vector
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Get-method to retrieve the value for x0.
   *
   * @return value of x0
   */
  public double getX0() {
    return x0;
  }

  /**
   * Get-method to retrieve the value for x1.
   *
   * @return value of x1
   */
  public double getX1() {
    return x1;
  }

  /**
   * Method to add the value of another vector onto the current vector.
   *
   * @param otherVector the vector to add
   * @return sum of the two vectors
   * @throws IllegalArgumentException if the otherVector is equal to null
   */
  public Vector2D add(Vector2D otherVector) throws IllegalArgumentException {
    checkVector(otherVector);
    double x0 = this.x0 + otherVector.getX0();
    double x1 = this.x1 + otherVector.getX1();
    return new Vector2D(x0, x1);
  }

  /**
   * Method to subtract the value of another vector from the current vector.
   *
   * @param otherVector the vector to subtract
   * @return difference of the two vectors
   * @throws IllegalArgumentException if the otherVector is equal to null
   */
  public Vector2D subtract(Vector2D otherVector) throws IllegalArgumentException {
    checkVector(otherVector);
    double x0 = this.x0 - otherVector.getX0();
    double x1 = this.x1 - otherVector.getX1();
    return new Vector2D(x0, x1);
  }

  /**
   * Method that checks if the parameter vector is equal to null or not, and throws if it is.
   *
   * @param vector 2D-vector to be checked
   * @throws IllegalArgumentException if the vector is equal to null
   */
  private void checkVector(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("There was an attempt of using a null-value for a vector");
    }
  }
}
