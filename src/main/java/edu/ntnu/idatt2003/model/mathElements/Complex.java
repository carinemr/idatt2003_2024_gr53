package edu.ntnu.idatt2003.model.mathElements;

import java.lang.Math;

/** Class that represents a complex number by inheriting from the Vector2D class. Also has a
 * method for calculating the square root of a Complex-object.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 * @see #Complex(double, double)
 * @see #sqrt()
 */
public class Complex extends Vector2D {

  /** Constructor for the Complex-class. The constructor initializes an object by calling the
   * constructor of Vector2D, and using the parameters realPart and imaginaryPart as arguments.
   *
   * @param realPart the real part of a complex number, representing a in "a + i * b"
   * @param imaginaryPart the imaginary part of a complex number, representing b in "a + i * b"
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /** Method for calculating the square root of a complex number.
   *
   * @return the square root of the complex number
   */
  public Complex sqrt() {
    double realPart = this.getX0();
    double imaginaryPart = this.getX1();

    double a = Math.sqrt(Math.pow(realPart, 2) + Math.pow(imaginaryPart, 2));

    double newRealPart = Math.sqrt((a + realPart) / 2);
    double newImaginaryPart = Math.signum(imaginaryPart) * Math.sqrt((a - realPart) / 2);

    return new Complex(newRealPart, newImaginaryPart);
  }
}
