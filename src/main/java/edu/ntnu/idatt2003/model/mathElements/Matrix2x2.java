package edu.ntnu.idatt2003.model.mathElements;

/** Class that represents a two by two matrix, thus with four elements.
 * This class contains a constructor for creating said matrix, as well as a method for multiplying
 * the Matrix2x2-object with a vector.
 *
 * @author Therese Synnøve Rondeel, Carine Margrethe Rondeel
 * @version 1.0
 * @see #Matrix2x2(double, double, double, double)
 * @see #multiply(Vector2D)
 */
public class Matrix2x2 {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /** Constructor for the Matrix2x2-class.
   * The constructor initializes an object of the Matrix2x2-class, with 4 parameters representing
   * the 4 elements of the 2x2 matrix.
   *
   * @param a00 represents the element in the upper left corner
   * @param a01 represents the element in the upper right corner
   * @param a10 represents the element in the bottom left corner
   * @param a11 represents the element in the bottom right corner
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /** Get-method for accessing a00.
   *
   * @return value of a00
   */
  public double getA00() {
    return a00;
  }

  /** Get-method for accessing a01.
   *
   * @return value of a01
   */
  public double getA01() {
    return a01;
  }

  /** Get-method for accessing a10.
   *
   * @return value of a10
   */
  public double getA10() {
    return a10;
  }

  /** Get-method for accessing a11.
   *
   * @return value of a11
   */
  public double getA11() {
    return a11;
  }

  /** Method for multiplying a 2x2 matrix with a 2D-vector, with the vector as the parameter.
   * Before calculating, the method controls whether the value of the parameter is null.
   *
   * @param vector 2D-vector to use for the multiplication
   * @return the result-vector from multiplying a matrix with another vector
   * @throws IllegalArgumentException if the vector-parameter is null
   */
  public Vector2D multiply(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("There was an attempt of using a null-value for a vector");
    }
    double x0 = a00 * vector.getX0() + a01 * vector.getX1();
    double x1 = a10 * vector.getX0() + a11 * vector.getX1();
    return new Vector2D(x0, x1);
  }
}
