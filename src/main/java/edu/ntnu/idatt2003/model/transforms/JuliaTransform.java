package edu.ntnu.idatt2003.model.transforms;

import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;

/**
 * Class that represents a julia transformation that implements the Transform2D interface.
 * This class contains a constructor that contains a complex point and sign, as well as a method for transforming
 * it based on another vector.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 * @see #JuliaTransform(Complex, int)
 * @see #transform(Vector2D)
 */

public class JuliaTransform implements Transform2D {
  private final Complex point;
  private final int sign;

  /**
   * Constructor that makes sure the point is not null, and sign is either -1 or 1. If point and sign are valid,
   * the transformation gets constructed with the given parameters.
   *
   * @param point the complex number
   * @param sign int that is either -1 or 1
   * @throws IllegalArgumentException if the point or sign is illegal
   */
  public JuliaTransform(Complex point, int sign) throws IllegalArgumentException {
    if (point == null || (sign != -1 && sign != 1)) {
      throw new IllegalArgumentException("Cannot create a Julia transformation due to null-value "
          + "parameters");
    }

    this.point = point;
    this.sign = sign;
  }

  /**
   * Get-method that retrieves the value of the complex point.
   *
   * @return the value of the point
   */
  public Complex getPoint() {
    return point;
  }

  /**
   * Get-method that retrieves the value of the sign.
   *
   * @return the value of sign
   */
  public int getSign() {
    return sign;
  }

  /**
   * Performs a julia transformation by calculating z-c, then creating a new complex with these numbers and then
   * calculating using the sqrt-method.
   *
   * @param point vector to be transformed
   * @return transformed vector
   * @throws IllegalArgumentException if the given point is null-value
   */
  @Override
  public Vector2D transform(Vector2D point) throws IllegalArgumentException {
    if (point == null) {
      throw new IllegalArgumentException("There was an attempt of using a null-value for a vector");
    }

    Vector2D zSubtractedByC = point.subtract(this.point);

    Complex radicand = new Complex(zSubtractedByC.getX0(), zSubtractedByC.getX1());
    Complex z = radicand.sqrt();

    return new Vector2D(z.getX0() * sign, z.getX1() * sign);
  }
}
