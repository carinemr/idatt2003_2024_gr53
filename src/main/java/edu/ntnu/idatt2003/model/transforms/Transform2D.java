package edu.ntnu.idatt2003.model.transforms;

import edu.ntnu.idatt2003.model.mathElements.Vector2D;

/** Interface for two-dimensional transformations. Contains the transform method that gets implemented in
 * AffineTransform2D and JuliaTransform.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 * @see #transform(Vector2D)
 */
public interface Transform2D {
  /**
   * Transforms a vector with the given point, depending on which implementation of Transform2D that was used.
   *
   * @param point vector to be transformed
   * @return transformed vector
   */
  Vector2D transform(Vector2D point);
}
