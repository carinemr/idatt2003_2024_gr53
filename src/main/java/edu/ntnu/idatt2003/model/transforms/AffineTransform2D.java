package edu.ntnu.idatt2003.model.transforms;

import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;

/** Class that represents an affine 2D transformation that implements the Transform2D interface.
 * This class contains a constructor that contains a valid matrix and vector, as well as a method for transforming
 * it based on another vector.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 * @see #AffineTransform2D(Matrix2x2, Vector2D)
 * @see #transform(Vector2D)
 */
public class AffineTransform2D implements Transform2D {
  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * Constructor that makes sure the matrix and vector parameters are not null. If not null, then the transformation
   * gets constructed with the given parameters.
   * @param matrix matrix
   * @param vector vector
   * @throws IllegalArgumentException if the matrix or vector are null-value parameters
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) throws IllegalArgumentException{
    if (matrix == null || vector == null) {
      throw new IllegalArgumentException("Cannot create an affine transformation due to null-value "
          + "parameters");
    }

    this.matrix = matrix;
    this.vector = vector;
  }

  /** Get-method for accessing the matrix.
   *
   * @return the matrix
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /** Get-method for accessing the vector.
   *
   * @return the vector
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * Performs an affine transformation by calculating Ax, which is the matrix product, and adding that onto the
   * pre-existing vector.
   *
   * @param point vector to be transformed
   * @return transformed vector
   * @throws IllegalArgumentException if the given point is null-value
   */
  @Override
  public Vector2D transform(Vector2D point) throws IllegalArgumentException {
    if (point == null) {
      throw new IllegalArgumentException("There was an attempt of using a null-value for a vector");
    }

    Vector2D Ax = matrix.multiply(point);

    return vector.add(Ax);
  }
}
