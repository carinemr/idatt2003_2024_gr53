package edu.ntnu.idatt2003.model;

import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import java.util.List;

/** Class that holds the description of a ChaosGame. This includes the minimum and maximum coords
 * of the fractal, as well as the transformations needed.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class ChaosGameDescription {
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transforms;

  /** Constructor for the ChaosGameDescription-class. It initializes a ChaosGameDescription-object
   * containing information such as minimum and maximum coords of the fractal, as well as the
   * transformations needed.
   *
   * @param transforms list of transformations
   * @param minCoords the minimum coordinates of the fractal
   * @param maxCoords the maximum coordinates of the fractal
   * @throws IllegalArgumentException if the list of transformations is empty, or if the coordinates
   *                                  are null
   */
  public ChaosGameDescription(List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    if (transforms.isEmpty()) {
      throw new IllegalArgumentException("Cannot create description because list of transforms "
          + "is empty or equal to null.");
    }
    if (minCoords == null || maxCoords == null) {
      throw new IllegalArgumentException("Cannot create description because min or max coords "
          + "are equal to null.");
    }

    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
  }

  /** Method to get the list of transformations for the description.
   *
   * @return a list of transformations
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /** Method to get the minimum coordinates for the description.
   *
   * @return the minimum coordinates
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /** Method to get the maximum coordinates for the description
   *
   * @return the maximum coordinates
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /** Method to set the minimum coordinates of the description.
   *
   * @param newMinCoords the new coordinates to be set
   * @throws IllegalArgumentException if the parameter is null or is greater than the maximum
   *                                  coordinates
   */
  public void setMinCoords(Vector2D newMinCoords) throws IllegalArgumentException {
    if (newMinCoords == null) {
      throw new IllegalArgumentException("Could not set new min coords due to new values being "
          + "null");
    }
    if (newMinCoords.getX0() >= maxCoords.getX0() || newMinCoords.getX1() >= maxCoords.getX1()) {
      throw new IllegalArgumentException("Could not set new min coords due to new x0 or x1 being "
          + "greater than the max coord counterpart");
    }

    this.minCoords = newMinCoords;
  }

  /** Method to set the maximum coordinates of the description.
   *
   * @param newMaxCoords the new coordinates to be set
   * @throws IllegalArgumentException if the parameter is null or is less than the minimum
   *                                  coordinates
   */
  public void setMaxCoords(Vector2D newMaxCoords) throws IllegalArgumentException {
    if (newMaxCoords == null) {
      throw new IllegalArgumentException("Could not set new min coords due to new values being "
          + "null");
    }
    if (newMaxCoords.getX0() <= minCoords.getX0() || newMaxCoords.getX1() <= minCoords.getX1()) {
      throw new IllegalArgumentException("Could not set new max coords due to new x0 or x1 being "
          + "lesser than the min coord counterpart");
    }

    this.maxCoords = newMaxCoords;
  }

  /** Method to set the whole list of transformations.
   *
   * @param newTransforms the new list with transformations
   * @throws IllegalArgumentException if the parameter is empty
   */
  public void setTransforms(List<Transform2D> newTransforms) throws IllegalArgumentException {
    if (newTransforms.isEmpty()) {
      throw new IllegalArgumentException("Could not set new transforms due to the new transform "
          + "list being empty");
    }

    this.transforms = newTransforms;
  }

  /** Method to set a specific transformation in the list of transformations.
   *
   * @param index the index of the transformation to be changed
   * @param newTransform the new transformation
   * @throws IllegalArgumentException if the new transformation is null, or the index is out of
   *                                  bounds of the list
   */
  public void setSpecificTransform(int index, Transform2D newTransform)
      throws IllegalArgumentException {
    if (newTransform == null) {
      throw new IllegalArgumentException("Could not set new transform as it was equal to null");
    }
    if (index > this.transforms.size() - 1 || index < 0) {
      throw new IllegalArgumentException("Index is out of bounds of the list of transforms");
    }

    this.transforms.remove(index);
    this.transforms.add(index, newTransform);
  }
}
