package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.ChaosGame;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.fileHandling.ChaosGameFileHandler;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * A class that contains a CLI application, to test out reading
 * from file and writing to file, from a text based interface.
 *
 * @author Carine Margrethe Rondeel, Therese Synnøve Rondeel
 * @version 1.0
 */
public class CLIApp {
  private static Scanner in;
  private static final int PRINT_ASCII_FRACTAL_TO_CONSOLE = 1;
  private static final int WRITE_FRACTAL_TO_FILE = 2;
  private static final int EXIT_APP = 3;
  private static String currentType = "";

  /**
   * Method that launches the text based application. Creates a menu,
   * to choose a fractal to run from file, write most recently ran
   * fractal to file, or to quit the application.
   *
   * @param args array of strings that contains the command line arguments
   * @throws IOException if reading or writing goes wrong
   */
  public static void main(String[] args) throws IOException {
    in = new Scanner(System.in);
    boolean exitMenu = false;

    while (!exitMenu) {
      mainMenuOptions();
      int option = scanInputInt();
      switch (option) {
        case PRINT_ASCII_FRACTAL_TO_CONSOLE -> printAsciiFractalToConsole();
        case WRITE_FRACTAL_TO_FILE -> writeFractalToNewFile();
        case EXIT_APP -> exitMenu = true;
        default -> System.out.println("Please choose a valid option!\n");
      }
    }
    in.close();
  }

  /**
   * Method that prints the main menu options.
   */
  private static void mainMenuOptions() {
    System.out.println("Choose what to do: ");
    System.out.println("1) Print ASCII fractal to console");
    System.out.println("2) Write most recently ran fractal to file");
    System.out.println("3) Quit");
  }

  /**
   * Method to write fractal to new file. Checks if the name is available,
   * if not it deletes the previous and creates a new one with the given
   * path.
   *
   * @throws IOException if writing goes wrong
   */
  private static void writeFractalToNewFile() throws IOException {
    if (!currentType.isEmpty()) {
      String path = "user-files/" + currentType + ".txt";
      String newSavePath = "user-files/" + currentType + "-cli-save.txt";

      File file = new File(newSavePath);
      if (file.exists()) {
        System.out.println("File with this name already exists. Deleting now.");
        file.delete();
        if (!file.exists()) {
          System.out.println("File successfully deleted!");
        } else  {
          System.out.println("Something went wrong, file was not deleted, and the writing fractal to file was stopped.");
        }
      }

      if (!file.exists()){
        ChaosGameDescription chaosGameDescription = ChaosGameFileHandler.readFromFile(path);
        ChaosGameFileHandler.writeToFile(chaosGameDescription, newSavePath);

        if (file.exists()) {
          System.out.println("File was successfully created!");
        } else {
          System.out.println("Something went wrong, file was not created.");
        }
      }
    } else {
      System.out.println("Could not save to file. Please run a fractal from the main menu before trying to " +
          "save to file again.\n");
    }
  }

  /**
   * Method that prints a menu of files available to run, and then
   * prints the ASCII fractal to the console based on the input.
   *
   * @throws IOException if something goes wrong while reading
   */
  public static void printAsciiFractalToConsole() throws IOException {
    ChaosGameDescription gameDescription;
    int steps = 500;

    System.out.println("Choose which file to read from (game will have 500 iterations):");
    System.out.println("1) Barnsley fern");
    System.out.println("2) Julia");
    System.out.println("3) Sierpinski");

    int option = scanInputInt();
    switch (option) {
      case 1 -> {
        gameDescription = ChaosGameFileHandler.readFromFile("user-files/barnsley-fern.txt");
        currentType = "barnsley-fern";
        runChaosGame(gameDescription, steps);
      }
      case 2 -> {
        gameDescription = ChaosGameFileHandler.readFromFile("user-files/julia.txt");
        currentType = "julia";
        runChaosGame(gameDescription, steps);
      }
      case 3 -> {
        gameDescription = ChaosGameFileHandler.readFromFile("user-files/sierpinski.txt");
        currentType = "sierpinski";
        runChaosGame(gameDescription, steps);
      }
    }
  }

  /**
   * Method for running the chaos game and display it on a canvas.
   *
   * @param description the description for the given fractal
   * @param steps amount of iterations to be run
   */
  public static void runChaosGame(ChaosGameDescription description, int steps) {
    int width = 50;
    int height = 30;
    ChaosGame chaosGame = new ChaosGame(description, width, height);

    chaosGame.runSteps(steps);
    int[][] canvas = chaosGame.getCanvas().getCanvasArray();

    StringBuilder printedCanvas = new StringBuilder();

    for (int[] row : canvas) {
      for (int value : row) {
        printedCanvas.append(value).append(" ");
      }
      printedCanvas.append("\n");
    }

    System.out.println(printedCanvas.toString());

  }

  /**
   * Method that scans the input to make sure the input is an integer.
   *
   * @return input that is an int
   */
  private static int scanInputInt() {
    while (!in.hasNextInt()) {
      System.out.println("Your input is not allowed, please input an integer and try "
          + "again: ");
      in.next();
    }
    int input = in.nextInt();
    in.nextLine();
    return input;
  }
}
