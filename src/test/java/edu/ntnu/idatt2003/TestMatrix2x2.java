package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestMatrix2x2 {
  private final double A00 = 0.5;
  private final double A01 =1;
  private final double A10 = 1;
  private final double A11 = 0.5;
  private final double VX0 = 1;
  private final double VX1 = 2;

  private Matrix2x2 matrix;
  private Vector2D vector;

  @BeforeEach
  public void setUp() {
    matrix = new Matrix2x2(A00, A01, A10, A11);
    vector = new Vector2D(VX0, VX1);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    assertNotNull(matrix);
    assertEquals(A00, matrix.getA00());
    assertEquals(A01, matrix.getA01());
    assertEquals(A10, matrix.getA10());
    assertEquals(A11, matrix.getA11());
  }

  @Test
  @DisplayName("Test multiply-method with valid input")
  public void testMultiplyWithValidInput() {
    double expectedX0 = 2.5;
    double expectedX1 = 2;

    Vector2D resultVector = matrix.multiply(vector);

    assertEquals(expectedX0, resultVector.getX0());
    assertEquals(expectedX1, resultVector.getX1());
  }

  @Test
  @DisplayName("Test multiply-method with invalid input")
  public void testMultiplyWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> matrix.multiply(null));
  }
}
