package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestChaosGameDescriptionFactory {

  @Test
  @DisplayName("Test Sierpinski factory description creation")
  public void testSierpinskiFactoryDescription() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);

    Matrix2x2 matrix1 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector1 = new Vector2D(0, 0);

    Matrix2x2 matrix2 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector2 = new Vector2D(.25, .5);

    Matrix2x2 matrix3 = new Matrix2x2(.5, 0, 0, .5);
    Vector2D vector3 = new Vector2D(.5, 0);

    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);

    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(transform1);
    transforms.add(transform2);
    transforms.add(transform3);

    ChaosGameDescription expectedSierpinski = new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameDescription factorySierpinski = ChaosGameDescriptionFactory.getDescription("SIERPINSKI");

    for (int i = 0; i < transforms.size(); i++) {
      AffineTransform2D expectedTransforms = (AffineTransform2D) expectedSierpinski.getTransforms().get(i);
      AffineTransform2D factoryTransforms = (AffineTransform2D) factorySierpinski.getTransforms().get(i);

      Matrix2x2 expectedMatrix = expectedTransforms.getMatrix();
      Matrix2x2 factoryMatrix = factoryTransforms.getMatrix();
      Vector2D expectedVector = expectedTransforms.getVector();
      Vector2D factoryVector = factoryTransforms.getVector();

      assertEquals(expectedMatrix.getA00(), factoryMatrix.getA00());
      assertEquals(expectedMatrix.getA01(), factoryMatrix.getA01());
      assertEquals(expectedMatrix.getA10(), factoryMatrix.getA10());
      assertEquals(expectedMatrix.getA11(), factoryMatrix.getA11());

      assertEquals(expectedVector.getX0(), factoryVector.getX0());
      assertEquals(expectedVector.getX1(), factoryVector.getX1());
    }

    assertEquals(expectedSierpinski.getMinCoords().getX0(), factorySierpinski.getMinCoords().getX0());
    assertEquals(expectedSierpinski.getMinCoords().getX1(), factorySierpinski.getMinCoords().getX1());

    assertEquals(expectedSierpinski.getMaxCoords().getX0(), factorySierpinski.getMaxCoords().getX0());
    assertEquals(expectedSierpinski.getMaxCoords().getX1(), factorySierpinski.getMaxCoords().getX1());
  }

  @Test
  @DisplayName("Test Barnsley fern factory description creation")
  public void testBarnsleyFernFactoryDescription() {
    Matrix2x2 matrix1 = new Matrix2x2(0, 0, 0, .16);
    Vector2D vector1 = new Vector2D(0, 0);

    Matrix2x2 matrix2 = new Matrix2x2(.85, .04, -.04, .85);
    Vector2D vector2 = new Vector2D(0, 1.6);

    Matrix2x2 matrix3 = new Matrix2x2(.2, -.26, .23, .22);
    Vector2D vector3 = new Vector2D(0, 1.6);

    Matrix2x2 matrix4 = new Matrix2x2(-.15, .28, .26, .24);
    Vector2D vector4 = new Vector2D(0, .44);

    Vector2D minCoords = new Vector2D(-2.65, 0);
    Vector2D maxCoords = new Vector2D(2.65, 10);

    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);
    AffineTransform2D transform4 = new AffineTransform2D(matrix4, vector4);

    List<Transform2D> transforms = new ArrayList<>();
    transforms.add(transform1);
    transforms.add(transform2);
    transforms.add(transform3);
    transforms.add(transform4);

    ChaosGameDescription expectedBarnsleyFern =
        new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameDescription factoryBarnsleyFern =
        ChaosGameDescriptionFactory.getDescription("bArNSLeY_FeRN");

    for (int i = 0; i < transforms.size(); i++) {
      AffineTransform2D expectedTransforms =
          (AffineTransform2D) expectedBarnsleyFern.getTransforms().get(i);
      AffineTransform2D factoryTransforms =
          (AffineTransform2D) factoryBarnsleyFern.getTransforms().get(i);

      Matrix2x2 expectedMatrix = expectedTransforms.getMatrix();
      Matrix2x2 factoryMatrix = factoryTransforms.getMatrix();
      Vector2D expectedVector = expectedTransforms.getVector();
      Vector2D factoryVector = factoryTransforms.getVector();

      assertEquals(expectedMatrix.getA00(), factoryMatrix.getA00());
      assertEquals(expectedMatrix.getA01(), factoryMatrix.getA01());
      assertEquals(expectedMatrix.getA10(), factoryMatrix.getA10());
      assertEquals(expectedMatrix.getA11(), factoryMatrix.getA11());

      assertEquals(expectedVector.getX0(), factoryVector.getX0());
      assertEquals(expectedVector.getX1(), factoryVector.getX1());
    }

    assertEquals(expectedBarnsleyFern.getMinCoords().getX0(),
        factoryBarnsleyFern.getMinCoords().getX0());
    assertEquals(expectedBarnsleyFern.getMinCoords().getX1(),
        factoryBarnsleyFern.getMinCoords().getX1());

    assertEquals(expectedBarnsleyFern.getMaxCoords().getX0(),
        factoryBarnsleyFern.getMaxCoords().getX0());
    assertEquals(expectedBarnsleyFern.getMaxCoords().getX1(),
        factoryBarnsleyFern.getMaxCoords().getX1());
  }

  @Test
  @DisplayName("Test Julia factory description creation")
  public void testJuliaFactoryDescription() {
    int[] signArray = {-1, 1};
    List<Transform2D> transforms = new ArrayList<>();

    Complex c = new Complex(-.74543, .11301);
    Vector2D minCoords = new Vector2D(-1.6, -1);
    Vector2D maxCoords = new Vector2D(1.6, 1);

    for (int value : signArray) {
      transforms.add(new JuliaTransform(c, value));
    }

    ChaosGameDescription expectedJulia =
        new ChaosGameDescription(transforms, minCoords, maxCoords);
    ChaosGameDescription factoryJulia =
        ChaosGameDescriptionFactory.getDescription("julia");

    for (int i = 0; i < transforms.size(); i++) {
      JuliaTransform expectedTransforms =
          (JuliaTransform) expectedJulia.getTransforms().get(i);
      JuliaTransform factoryTransforms =
          (JuliaTransform) factoryJulia.getTransforms().get(i);

      Complex expectedPoint = expectedTransforms.getPoint();
      Complex factoryPoint = factoryTransforms.getPoint();
      int expectedSign = expectedTransforms.getSign();
      int factorySign = factoryTransforms.getSign();

      assertEquals(expectedPoint.getX0(), factoryPoint.getX0());
      assertEquals(expectedPoint.getX1(), factoryPoint.getX1());

      assertEquals(expectedSign, factorySign);
    }

    assertEquals(expectedJulia.getMinCoords().getX0(),
        factoryJulia.getMinCoords().getX0());
    assertEquals(expectedJulia.getMinCoords().getX1(),
        factoryJulia.getMinCoords().getX1());

    assertEquals(expectedJulia.getMaxCoords().getX0(),
        factoryJulia.getMaxCoords().getX0());
    assertEquals(expectedJulia.getMaxCoords().getX1(),
        factoryJulia.getMaxCoords().getX1());
  }

  @Test
  @DisplayName("Test factory creation with invalid input")
  public void testFactoryCreationInvalidInput() {
    String invalidString = "Invalid_fractal_name";

    assertThrows(IllegalArgumentException.class, () -> ChaosGameDescriptionFactory.getDescription(invalidString));
  }
}
