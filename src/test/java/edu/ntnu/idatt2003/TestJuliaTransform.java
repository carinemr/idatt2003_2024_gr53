package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestJuliaTransform {
  private final double cRealPart = 0.3;
  private final double cImaginaryPart = 0.6;
  private final int sign = 1;
  private final double zRealPart = 0.4;
  private final double zImaginaryPart = 0.2;
  private Complex c;
  private Complex z;
  private JuliaTransform juliaTransform;

  @BeforeEach
  public void setUp() {
    c = new Complex(cRealPart, cImaginaryPart);
    z = new Complex(zRealPart, zImaginaryPart);

    juliaTransform = new JuliaTransform(c, sign);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    assertEquals(cRealPart, juliaTransform.getPoint().getX0());
    assertEquals(cImaginaryPart, juliaTransform.getPoint().getX1());
    assertEquals(sign, juliaTransform.getSign());
    assertNotNull(juliaTransform);
  }

  @Test
  @DisplayName("Test constructor with invalid input")
  public void testConstructorWithInvalidInput() {
    Complex invalidComplex = null;
    int invalidSign = 0;

    assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(invalidComplex, sign));
    assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(c, invalidSign));
  }

  @Test
  @DisplayName("Test transform-method with valid input")
  public void testTransformWithValidInput() {
    double expectedRealPart = 0.506;
    double expectedImaginaryPart = -0.395;

    Vector2D resultComplex = juliaTransform.transform(z);

    assertEquals(expectedRealPart, Precision.round(resultComplex.getX0(), 3));
    assertEquals(expectedImaginaryPart, Precision.round(resultComplex.getX1(), 3));
  }

  @Test
  @DisplayName("Test transform-method with invalid input")
  public void testTransformWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> juliaTransform.transform(null));
  }
}
