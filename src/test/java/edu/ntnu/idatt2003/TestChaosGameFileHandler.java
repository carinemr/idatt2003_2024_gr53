package edu.ntnu.idatt2003;

import static edu.ntnu.idatt2003.model.fileHandling.ChaosGameFileHandler.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.factory.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.io.IOException;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestChaosGameFileHandler {
  String emptyString = "";

  @Test
  @DisplayName("Test readFromFile with invalid input")
  public void testReadFromFileWithInvalidInput() {
    String invalidPath = "/resources/notTxt.css";

    assertThrows(IOException.class, () -> readFromFile(invalidPath));
  }

  @Test
  @DisplayName("Test cleanString with valid input")
  public void testCleanStringWithValidInput() {
    String testString = "Julia # Type of transform";
    String expectedOutput = "Julia";

    String actualOutput = cleanString(testString);

    assertEquals(expectedOutput, actualOutput);
  }

  @Test
  @DisplayName("Test cleanString with invalid input")
  public void testCleanStringWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> cleanString(emptyString));
  }

  @Test
  @DisplayName("Test parseVector with valid input")
  public void testParseVectorWithValidInput() {
    String validLine = "-1.6, -1      # Lower left";
    Vector2D expectedVector = new Vector2D(-1.6, -1);

    Vector2D actualVector = parseVector(validLine);

    assertEquals(expectedVector.getX0(), actualVector.getX0());
    assertEquals(expectedVector.getX1(), actualVector.getX1());
  }

  @Test
  @DisplayName("Test parseVector with invalid input")
  public void testParseVectorWithInvalidInput() {
    String stringTooManyValues = "-1.6, -1, -1.3      # Lower left";
    String stringTooFewValues = "-1.6     # Lower left";
    String stringNoNumbers = "a, b    # Lower left";

    assertThrows(IllegalArgumentException.class, () -> parseVector(emptyString));
    assertThrows(IllegalArgumentException.class, () -> parseVector(stringTooManyValues));
    assertThrows(NoSuchElementException.class, () -> parseVector(stringTooFewValues));
    assertThrows(NoSuchElementException.class, () -> parseVector(stringNoNumbers));
  }

  @Test
  @DisplayName("Test parseAffine2DTransform with valid input")
  public void testParseAffine2DTransformWithValidInput() {
    String validLine = ".5, 0, 0, .5, 0, 0";

    Matrix2x2 expectedMatrix = new Matrix2x2(0.5, 0, 0, 0.5);
    Vector2D expectedVector = new Vector2D(0, 0);
    AffineTransform2D expectedTransform = new AffineTransform2D(expectedMatrix, expectedVector);

    AffineTransform2D actualTransform = (AffineTransform2D) parseAffine2DTransform(validLine);

    assertEquals(expectedTransform.getMatrix().getA00(), actualTransform.getMatrix().getA00());
    assertEquals(expectedTransform.getMatrix().getA01(), actualTransform.getMatrix().getA01());
    assertEquals(expectedTransform.getMatrix().getA10(), actualTransform.getMatrix().getA10());
    assertEquals(expectedTransform.getMatrix().getA11(), actualTransform.getMatrix().getA11());

    assertEquals(expectedTransform.getVector().getX0(), actualTransform.getVector().getX0());
    assertEquals(expectedTransform.getVector().getX1(), actualTransform.getVector().getX1());
  }

  @Test
  @DisplayName("Test parseAffine2DTransform with invalid input")
  public void testParseAffine2DTransformWithInvalidInput() {
    String stringTooManyValues = ".5, 0, 0, .5, 0, 0, 0    # 1st transform";
    String stringTooFewValues = ".5, 0, 0    # 1st transform";
    String stringNoNumbers = "a, b, c, d, e, f ";

    assertThrows(IllegalArgumentException.class, () -> parseAffine2DTransform(emptyString));
    assertThrows(IllegalArgumentException.class, () -> parseAffine2DTransform(stringTooManyValues));
    assertThrows(NoSuchElementException.class, () -> parseAffine2DTransform(stringTooFewValues));
    assertThrows(NoSuchElementException.class, () -> parseAffine2DTransform(stringNoNumbers));
  }

  @Test
  @DisplayName("Test parseJuliaTransform with valid input")
  public void testParseJuliaTransformWithValidInput() {
    int sign = -1;
    String validLine = "-.74543, .11301  # Real and imaginary";
    Complex c = new Complex(-.74543, .11301);

    JuliaTransform expectedTransform = new JuliaTransform(c, sign);
    JuliaTransform actualTransform = (JuliaTransform) parseJuliaTransform(validLine, sign);

    assertEquals(expectedTransform.getPoint().getX0(), actualTransform.getPoint().getX0());
    assertEquals(expectedTransform.getPoint().getX1(), actualTransform.getPoint().getX1());
    assertEquals(expectedTransform.getSign(), actualTransform.getSign());
  }

  @Test
  @DisplayName("Test parseJuliaTransform with invalid input")
  public void testParseJuliaTransformWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> parseJuliaTransform(emptyString, 1));
  }

  @Test
  @DisplayName("Test writeToFile with invalid input")
  public void testWriteToFileWithInvalidInput() {
    String invalidPath = "/resources/notTxt.css";
    ChaosGameDescription invalidDescription = null;
    ChaosGameDescription validDescription = ChaosGameDescriptionFactory.getDescription("sierpinski");

    //the first assert will catch the description being wrong and throw, before checking the path
    assertThrows(IllegalArgumentException.class, () -> writeToFile(invalidDescription, invalidPath));
    assertThrows(Exception.class, () -> writeToFile(validDescription, invalidPath));
  }
}
