package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.mathElements.Complex;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestComplex {
  private final double realPart = 0.1;
  private final double imaginaryPart = -0.4;
  private Complex complex;

  @BeforeEach
  public void setUp() {
    complex = new Complex(realPart, imaginaryPart);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    assertNotNull(complex);
    assertEquals(realPart, complex.getX0());
    assertEquals(imaginaryPart, complex.getX1());
  }

  @Test
  @DisplayName("Test sqrt-method with valid input up to three decimals")
  public void testSqrtWithValidInput() {
    double expectedRealPart = 0.506;
    double expectedImaginaryPart = -0.395;

    Complex resultComplex = complex.sqrt();

    assertEquals(expectedRealPart, Precision.round(resultComplex.getX0(), 3));
    assertEquals(expectedImaginaryPart, Precision.round(resultComplex.getX1(), 3));
  }
}
