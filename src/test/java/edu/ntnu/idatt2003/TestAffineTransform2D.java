package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestAffineTransform2D {
  private final double A00 = 0.5;
  private final double A01 =1;
  private final double A10 = 1;
  private final double A11 = 0.5;
  private final double XX0 = 1;
  private final double XX1 = 2;
  private final double BX0 = 3;
  private final double BX1 = 1;

  private Matrix2x2 matrix;
  private Vector2D xVector;
  private Vector2D bVector;
  private AffineTransform2D affineTransform;

  @BeforeEach
  public void setUp() {
    matrix = new Matrix2x2(A00, A01, A10, A11);
    xVector = new Vector2D(XX0, XX1);
    bVector = new Vector2D(BX0, BX1);
    affineTransform = new AffineTransform2D(matrix, bVector);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    Matrix2x2 affineMatrix = affineTransform.getMatrix();
    Vector2D affineVector = affineTransform.getVector();

    assertEquals(A00, affineMatrix.getA00());
    assertEquals(A01, affineMatrix.getA01());
    assertEquals(A10, affineMatrix.getA10());
    assertEquals(A11, affineMatrix.getA11());
    assertEquals(BX0, affineVector.getX0());
    assertEquals(BX1, affineVector.getX1());
    assertNotNull(affineTransform);
  }

  @Test
  @DisplayName("Test constructor with invalid input")
  public void testConstructorWithInvalidInput() {
    Matrix2x2 invalidMatrix = null;
    Vector2D invalidVector = null;

    assertThrows(IllegalArgumentException.class,
        () -> new AffineTransform2D(invalidMatrix, bVector));
    assertThrows(IllegalArgumentException.class,
        () -> new AffineTransform2D(matrix, invalidVector));
  }

  @Test
  @DisplayName("Test transform-method with valid input")
  public void testTransformWithValidInput() {
    double expectedX0 = 5.5;
    double expectedX1 = 3;

    Vector2D resultVector = affineTransform.transform(xVector);

    assertEquals(expectedX0, resultVector.getX0());
    assertEquals(expectedX1, resultVector.getX1());
  }

  @Test
  @DisplayName("Test transform-method with invalid input")
  public void testTransformWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> affineTransform.transform(null));
  }
}
