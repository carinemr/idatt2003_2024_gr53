package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestChaosGameDescription {
  Vector2D minCoords;
  Vector2D maxCoords;
  List<Transform2D> juliaTransforms;
  Complex c;
  JuliaTransform juliaTransformPos;
  JuliaTransform juliaTransformNeg;
  ChaosGameDescription description;
  ChaosGameDescription sierpinskiDescription;
  Vector2D vector2;
  List<Transform2D> emptyTransformsList = new ArrayList<>();


  @BeforeEach
  public void setUp() {
    //Julia setup

    minCoords = new Vector2D(-1.6, -1);
    maxCoords = new Vector2D(1.6, 1);
    juliaTransforms = new ArrayList<>();

    c = new Complex(-0.74543, 0.11301);

    juliaTransformPos = new JuliaTransform(c, 1);
    juliaTransformNeg = new JuliaTransform(c, -1);

    juliaTransforms.add(juliaTransformPos);
    juliaTransforms.add(juliaTransformNeg);

    description = new ChaosGameDescription(juliaTransforms, minCoords, maxCoords);

    Vector2D minCoordsAffine = new Vector2D(0, 0);
    Vector2D maxCoordsAffine = new Vector2D(1, 1);

    //Sierpinski setup
    Matrix2x2 matrix1 = new Matrix2x2(.5, 0, 0, .5);
    Matrix2x2 matrix2 = new Matrix2x2(.5, 0, 0, .5);
    Matrix2x2 matrix3 = new Matrix2x2(.5, 0, 0, .5);

    Vector2D vector1 = new Vector2D(0, 0);
    vector2 = new Vector2D(0.25, 0.5);
    Vector2D vector3 = new Vector2D(0.5, 0);

    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);

    List<Transform2D> sierpinskiTransforms = new ArrayList<>();
    sierpinskiTransforms.add(transform1);
    sierpinskiTransforms.add(transform2);
    sierpinskiTransforms.add(transform3);

    sierpinskiDescription = new ChaosGameDescription(sierpinskiTransforms, minCoordsAffine,
        maxCoordsAffine);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    assertEquals(minCoords, description.getMinCoords());
    assertEquals(maxCoords, description.getMaxCoords());
    assertEquals(juliaTransforms, description.getTransforms());
  }

  @Test
  @DisplayName("Test constructor with invalid input")
  public void testConstructorWithInvalidInput() {
    Vector2D minCoordsNull = null;
    Vector2D maxCoordsNull = null;

    assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(emptyTransformsList, minCoords,
        maxCoords));
    assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(juliaTransforms, minCoordsNull,
        maxCoords));
    assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(juliaTransforms, minCoords,
        maxCoordsNull));
  }

  @Test
  @DisplayName("Test setMinCoords-method with valid input")
  public void testSetMinCoordsWithValidInput() {
    Vector2D newMinCoords = new Vector2D(minCoords.getX0(), -1.2);

    description.setMinCoords(newMinCoords);

    assertEquals(newMinCoords, description.getMinCoords());
  }

  @Test
  @DisplayName("Test setMinCoords-method with invalid input")
  public void testSetMinCoordsWithInvalidInput() {
    Vector2D greaterX0ThanMaxCoords = new Vector2D(maxCoords.getX0() + 0.2, minCoords.getX1());
    Vector2D greaterX1ThanMaxCoords = new Vector2D(minCoords.getX0(), maxCoords.getX1() + 0.2);

    assertThrows(IllegalArgumentException.class, () -> description.setMinCoords(null));
    assertThrows(IllegalArgumentException.class, () -> description.setMinCoords(greaterX0ThanMaxCoords));
    assertThrows(IllegalArgumentException.class, () -> description.setMinCoords(greaterX1ThanMaxCoords));
  }

  @Test
  @DisplayName("Test setMaxCoords-method with valid input")
  public void testSetMaxCoordsWithValidInput() {
    Vector2D newMaxCoords = new Vector2D(1.8, maxCoords.getX1());

    description.setMaxCoords(newMaxCoords);

    assertEquals(newMaxCoords, description.getMaxCoords());
  }

  @Test
  @DisplayName("Test setMaxCoords-method with invalid input")
  public void testSetMaxCoordsWithInvalidInput() {
    Vector2D lesserX0ThanMinCoords = new Vector2D(maxCoords.getX0(), minCoords.getX1() - 0.2);
    Vector2D lesserX1ThanMinCoords = new Vector2D(minCoords.getX0() - 0.2, maxCoords.getX1());

    assertThrows(IllegalArgumentException.class, () -> description.setMaxCoords(null));
    assertThrows(IllegalArgumentException.class, () -> description.setMaxCoords(lesserX0ThanMinCoords));
    assertThrows(IllegalArgumentException.class, () -> description.setMaxCoords(lesserX1ThanMinCoords));
  }

  @Test
  @DisplayName("Test setTransforms with valid input")
  public void testSetTransformsWithValidInput() {
    Complex newComplex = new Complex(-0.8, 0.2);

    JuliaTransform newTransformPos = new JuliaTransform(newComplex, -1);
    JuliaTransform newTransformNeg = new JuliaTransform(newComplex, 1);

    List<Transform2D> newTransforms = new ArrayList<>();
    newTransforms.add(newTransformPos);
    newTransforms.add(newTransformNeg);

    description.setTransforms(newTransforms);

    assertEquals(newTransforms, description.getTransforms());
  }

  @Test
  @DisplayName("Test setTransforms with invalid input")
  public void testSetTransformsWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> description.setTransforms(
        emptyTransformsList));
  }

  @Test
  @DisplayName("Test setSpecificTransform with valid input")
  public void testSetSpecificTransformWithValidInput() {
    Matrix2x2 newMatrix = new Matrix2x2(0.3, 0, 0, 0.5);
    int index = sierpinskiDescription.getTransforms().size() - 1;
    AffineTransform2D newTransformation = new AffineTransform2D(newMatrix, vector2);

    sierpinskiDescription.setSpecificTransform(index, newTransformation);

    assertEquals(newTransformation, sierpinskiDescription.getTransforms().get(index));
  }

  @Test
  @DisplayName("Test setSpecificTransform with invalid input")
  public void testSetSpecificTransformWithInvalidInput() {
    int validIndex = sierpinskiDescription.getTransforms().size() - 1;
    int invalidIndex = sierpinskiDescription.getTransforms().size() + 1;
    Matrix2x2 newMatrix = new Matrix2x2(0.3, 0, 0, 0.5);
    AffineTransform2D newValidTransformation = new AffineTransform2D(newMatrix, vector2);
    AffineTransform2D newInvalidTransformation = null;

    assertThrows(IllegalArgumentException.class, () -> description.setSpecificTransform(validIndex,
        newInvalidTransformation));
    assertThrows(IllegalArgumentException.class, () -> description.setSpecificTransform(invalidIndex,
        newValidTransformation));
  }
}
