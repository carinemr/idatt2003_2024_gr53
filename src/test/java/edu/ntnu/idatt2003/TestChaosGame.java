package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosCanvas;
import edu.ntnu.idatt2003.model.ChaosGame;
import edu.ntnu.idatt2003.model.ChaosGameDescription;
import edu.ntnu.idatt2003.model.mathElements.Complex;
import edu.ntnu.idatt2003.model.transforms.JuliaTransform;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.transforms.Transform2D;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestChaosGame {
  int validWidth = 50;
  int validHeight = 50;
  Vector2D minCoords;
  Vector2D maxCoords;
  List<Transform2D> juliaTransforms;
  Complex c;
  JuliaTransform juliaTransformPos;
  JuliaTransform juliaTransformNeg;
  ChaosGameDescription description;
  ChaosGameDescription sierpinskiDescription;
  Vector2D vector2;
  List<Transform2D> emptyTransformsList = new ArrayList<>();
  ChaosGame juliaChaosGame;

  @BeforeEach
  public void setUp() {

    //Julia setup

    minCoords = new Vector2D(-1.6, -1);
    maxCoords = new Vector2D(1.6, 1);
    juliaTransforms = new ArrayList<>();

    c = new Complex(-0.74543, 0.11301);

    juliaTransformPos = new JuliaTransform(c, 1);
    juliaTransformNeg = new JuliaTransform(c, -1);

    juliaTransforms.add(juliaTransformPos);
    juliaTransforms.add(juliaTransformNeg);

    description = new ChaosGameDescription(juliaTransforms, minCoords, maxCoords);

    Vector2D minCoordsAffine = new Vector2D(0, 0);
    Vector2D maxCoordsAffine = new Vector2D(1, 1);

    //Sierpinski setup
    Matrix2x2 matrix1 = new Matrix2x2(.5, 0, 0, .5);
    Matrix2x2 matrix2 = new Matrix2x2(.5, 0, 0, .5);
    Matrix2x2 matrix3 = new Matrix2x2(.5, 0, 0, .5);

    Vector2D vector1 = new Vector2D(0, 0);
    vector2 = new Vector2D(0.25, 0.5);
    Vector2D vector3 = new Vector2D(0.5, 0);

    AffineTransform2D transform1 = new AffineTransform2D(matrix1, vector1);
    AffineTransform2D transform2 = new AffineTransform2D(matrix2, vector2);
    AffineTransform2D transform3 = new AffineTransform2D(matrix3, vector3);

    List<Transform2D> sierpinskiTransforms = new ArrayList<>();
    sierpinskiTransforms.add(transform1);
    sierpinskiTransforms.add(transform2);
    sierpinskiTransforms.add(transform3);

    sierpinskiDescription = new ChaosGameDescription(sierpinskiTransforms, minCoordsAffine,
        maxCoordsAffine);

    juliaChaosGame = new ChaosGame(description, validWidth, validHeight);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    ChaosGame sierpinskiGame = new ChaosGame(sierpinskiDescription, validWidth, validHeight);
    Vector2D expectedCurrentPoint = new Vector2D(0, 0);
    ChaosCanvas expectedCanvas = new ChaosCanvas(validWidth, validHeight,
        sierpinskiDescription.getMinCoords(), sierpinskiDescription.getMaxCoords());

    assertEquals(sierpinskiDescription, sierpinskiGame.getDescription());

    assertEquals(expectedCurrentPoint.getX0(), sierpinskiGame.getCurrentPoint().getX0());
    assertEquals(expectedCurrentPoint.getX1(), sierpinskiGame.getCurrentPoint().getX1());

    assertEquals(expectedCanvas.getWidth(), sierpinskiGame.getCanvas().getWidth());
    assertEquals(expectedCanvas.getHeight(), sierpinskiGame.getCanvas().getHeight());
    assertEquals(expectedCanvas.getMinCoords(), sierpinskiGame.getCanvas().getMinCoords());
    assertEquals(expectedCanvas.getMaxCoords(), sierpinskiGame.getCanvas().getMaxCoords());
  }

  @Test
  @DisplayName("Test constructor with invalid input")
  public void testConstructorWithInvalidInput() {
    int invalidWidth = -1;
    int invalidHeight = -5;

    assertThrows(IllegalArgumentException.class, () -> new ChaosGame(null, validWidth,
        validHeight));
    assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description,
        invalidWidth, validHeight));
    assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description,
        validWidth, invalidHeight));
  }

  @Test
  @DisplayName("Test setTransforms with valid input")
  public void testSetTransformsWithValidInput() {
    Complex newComplex = new Complex(-0.9, 0.3);

    JuliaTransform newTransformPos = new JuliaTransform(newComplex, -1);
    JuliaTransform newTransformNeg = new JuliaTransform(newComplex, 1);

    List<Transform2D> newTransforms = new ArrayList<>();
    newTransforms.add(newTransformPos);
    newTransforms.add(newTransformNeg);

    description.setTransforms(newTransforms);

    assertEquals(newTransforms, description.getTransforms());
  }

  @Test
  @DisplayName("Test setTransforms with invalid input")
  public void testSetTransformsWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> description.setTransforms(
        emptyTransformsList));
  }

  @Test
  @DisplayName("Test setSpecificTransform with valid input")
  public void testSetSpecificTransformWithValidInput() {
    Matrix2x2 newMatrix = new Matrix2x2(0.4, 0, 0, 0.5);
    int index = sierpinskiDescription.getTransforms().size() - 1;
    AffineTransform2D newTransformation = new AffineTransform2D(newMatrix, vector2);

    sierpinskiDescription.setSpecificTransform(index, newTransformation);

    assertEquals(newTransformation, sierpinskiDescription.getTransforms().get(index));
  }

  @Test
  @DisplayName("Test setSpecificTransform with invalid input")
  public void testSetSpecificTransformWithInvalidInput() {
    int validIndex = sierpinskiDescription.getTransforms().size() - 1;
    int invalidIndex = sierpinskiDescription.getTransforms().size() + 1;
    Matrix2x2 newMatrix = new Matrix2x2(0.2, 0, 0, 0.5);
    AffineTransform2D newValidTransformation = new AffineTransform2D(newMatrix, vector2);
    AffineTransform2D newInvalidTransformation = null;

    assertThrows(IllegalArgumentException.class, () -> description.setSpecificTransform(validIndex,
        newInvalidTransformation));
    assertThrows(IllegalArgumentException.class, () ->
        description.setSpecificTransform(invalidIndex, newValidTransformation));
  }

  @Test
  @DisplayName("Test setMinCoords-method with valid input")
  public void testSetMinCoordsWithValidInput() {
    Vector2D newMinCoords = new Vector2D(minCoords.getX0(), -1.3);

    description.setMinCoords(newMinCoords);

    assertEquals(newMinCoords, description.getMinCoords());
  }

  @Test
  @DisplayName("Test setMinCoords-method with invalid input")
  public void testSetMinCoordsWithInvalidInput() {
    Vector2D greaterX0ThanMaxCoords = new Vector2D(maxCoords.getX0() + 0.1, minCoords.getX1());
    Vector2D greaterX1ThanMaxCoords = new Vector2D(minCoords.getX0(), maxCoords.getX1() + 0.3);

    assertThrows(IllegalArgumentException.class, () -> description.setMinCoords(null));
    assertThrows(IllegalArgumentException.class, () ->
        description.setMinCoords(greaterX0ThanMaxCoords));
    assertThrows(IllegalArgumentException.class, () ->
        description.setMinCoords(greaterX1ThanMaxCoords));
  }

  @Test
  @DisplayName("Test setMaxCoords-method with valid input")
  public void testSetMaxCoordsWithValidInput() {
    Vector2D newMaxCoords = new Vector2D(1.7, maxCoords.getX1());

    description.setMaxCoords(newMaxCoords);

    assertEquals(newMaxCoords, description.getMaxCoords());
  }

  @Test
  @DisplayName("Test setMaxCoords-method with invalid input")
  public void testSetMaxCoordsWithInvalidInput() {
    Vector2D lesserX0ThanMinCoords = new Vector2D(maxCoords.getX0(), minCoords.getX1() - 0.3);
    Vector2D lesserX1ThanMinCoords = new Vector2D(minCoords.getX0() - 0.1, maxCoords.getX1());

    assertThrows(IllegalArgumentException.class, () -> description.setMaxCoords(null));
    assertThrows(IllegalArgumentException.class, () ->
        description.setMaxCoords(lesserX0ThanMinCoords));
    assertThrows(IllegalArgumentException.class, () ->
        description.setMaxCoords(lesserX1ThanMinCoords));
  }

  @Test
  @DisplayName("Test runSteps with invalid steps")
  public void testRunStepsWithInvalidSteps() {
    int invalidSteps = -182;

    assertThrows(IllegalArgumentException.class, () -> juliaChaosGame.runSteps(invalidSteps));
  }
}
