package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestVector2D {

  private final double V1X0 = 3;
  private final double V1X1 = 4;
  private final double V2X0 = 3;
  private final double V2X1 = 1;

  private Vector2D vector1;
  private Vector2D vector2;

  @BeforeEach
  public void setUp() {
    vector1 = new Vector2D(V1X0, V1X1);
    vector2 = new Vector2D(V2X0, V2X1);
  }

  @Test
  @DisplayName("Test constructor with valid input")
  public void testConstructorWithValidInput() {
    assertEquals(V1X0, vector1.getX0());
    assertEquals(V1X1, vector1.getX1());
  }

  @Test
  @DisplayName("Test add-method with valid input")
  public void testAddWithValidInput() {
    double expectedX0 = V1X0 + V2X0;
    double expectedX1 = V1X1 + V2X1;

    Vector2D resultVector = vector1.add(vector2);

    assertEquals(expectedX0, resultVector.getX0());
    assertEquals(expectedX1, resultVector.getX1());
  }

  @Test
  @DisplayName("Test add-method with invalid input")
  public void testAddWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> vector1.add(null));
  }

  @Test
  @DisplayName("Test subtract-method with valid input")
  public void testSubtractWithValidInput() {
    double expectedX0 = V1X0 - V2X0;
    double expectedX1 = V1X1 - V2X1;

    Vector2D resultVector = vector1.subtract(vector2);

    assertEquals(expectedX0, resultVector.getX0());
    assertEquals(expectedX1, resultVector.getX1());
  }

  @Test
  @DisplayName("Test subtract-method with invalid input")
  public void testSubtractWithInvalidInput() {
    assertThrows(IllegalArgumentException.class, () -> vector1.subtract(null));
  }
}
