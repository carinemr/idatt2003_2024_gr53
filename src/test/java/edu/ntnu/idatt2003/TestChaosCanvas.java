package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.transforms.AffineTransform2D;
import edu.ntnu.idatt2003.model.ChaosCanvas;
import edu.ntnu.idatt2003.model.mathElements.Matrix2x2;
import edu.ntnu.idatt2003.model.mathElements.Vector2D;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestChaosCanvas {
  int validWidth = 50;
  int validHeight = 50;
  Vector2D validMinCoords = new Vector2D(0, 0);
  Vector2D validMaxCoords = new Vector2D(1, 1);
  ChaosCanvas validCanvas;

  @BeforeEach
  public void setUp() {
    validCanvas = new ChaosCanvas(validWidth, validHeight, validMinCoords, validMaxCoords);
  }

  @Test
  @DisplayName("Test constructor and validateCanvasInput with valid input")
  public void testConstructorAndValidateCanvasInputWithValidInput() {
    int[][] expectedCanvas = new int[validHeight][validWidth];

    assertEquals(validWidth, validCanvas.getWidth());
    assertEquals(validHeight, validCanvas.getHeight());
    assertEquals(validMinCoords, validCanvas.getMinCoords());
    assertEquals(validMaxCoords, validCanvas.getMaxCoords());

    assertArrayEquals(expectedCanvas, validCanvas.getCanvasArray());
  }

  @Test
  @DisplayName("Test constructor and validateCanvasInput with invalid input")
  public void testConstructorAndValidateCanvasInputWithInvalidInput() {
    int invalidWidth = -5;
    int invalidHeight = 0;
    Vector2D minCoordsMoreThanMax = new Vector2D(validMaxCoords.getX0() + 1, validMinCoords.getX1());
    Vector2D maxCoordsLessThanMin = new Vector2D(validMaxCoords.getX0(), validMinCoords.getX1() - 1);

    assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(invalidWidth, validHeight, validMinCoords,
        validMaxCoords));
    assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(validWidth, invalidHeight, validMinCoords,
        validMaxCoords));
    assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(validWidth, validHeight, minCoordsMoreThanMax,
        validMaxCoords));
    assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(validWidth, validHeight, validMinCoords,
        maxCoordsLessThanMin));
  }

  @Test
  @DisplayName("Test setMinCoords-method with valid input")
  public void testSetMinCoordsWithValidInput() {
    Vector2D newMinCoords = new Vector2D(validMinCoords.getX0(), -1.2);

    validCanvas.setMinCoords(newMinCoords);

    assertEquals(newMinCoords, validCanvas.getMinCoords());
  }

  @Test
  @DisplayName("Test setMinCoords-method with invalid input")
  public void testSetMinCoordsWithInvalidInput() {
    Vector2D greaterX0ThanMaxCoords = new Vector2D(validMaxCoords.getX0() + 0.2, validMinCoords.getX1());
    Vector2D greaterX1ThanMaxCoords = new Vector2D(validMinCoords.getX0(), validMaxCoords.getX1() + 0.2);

    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMinCoords(null));
    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMinCoords(greaterX0ThanMaxCoords));
    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMinCoords(greaterX1ThanMaxCoords));
  }

  @Test
  @DisplayName("Test setMaxCoords-method with valid input")
  public void testSetMaxCoordsWithValidInput() {
    Vector2D newMaxCoords = new Vector2D(1.8, validMaxCoords.getX1());

    validCanvas.setMaxCoords(newMaxCoords);

    assertEquals(newMaxCoords, validCanvas.getMaxCoords());
  }

  @Test
  @DisplayName("Test setMaxCoords-method with invalid input")
  public void testSetMaxCoordsWithInvalidInput() {
    Vector2D lesserX0ThanMinCoords = new Vector2D(validMaxCoords.getX0(), validMinCoords.getX1() - 0.2);
    Vector2D lesserX1ThanMinCoords = new Vector2D(validMinCoords.getX0() - 0.2, validMaxCoords.getX1());

    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMaxCoords(null));
    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMaxCoords(lesserX0ThanMinCoords));
    assertThrows(IllegalArgumentException.class, () -> validCanvas.setMaxCoords(lesserX1ThanMinCoords));
  }

  @Test
  @DisplayName("Test constructor and validateCanvasInput with valid input")
  public void testCalculateTransformCoordToIndicesWithValidInput() {
    double expectedA00 = 0, expectedA11 = 0;
    double expectedA01 = (validHeight-1)/(validMinCoords.getX1()-validMaxCoords.getX1());
    double expectedA10 = (validWidth-1)/(validMaxCoords.getX0() - validMinCoords.getX0());

    double expectedB0 = (validHeight - 1) * validMaxCoords.getX1() / (validMaxCoords.getX1() - validMinCoords.getX1());
    double expectedB1 = (validWidth - 1) * validMinCoords.getX0() / (validMinCoords.getX0() - validMaxCoords.getX0());

    AffineTransform2D transform = validCanvas.calculateTransformCoordToIndices(validWidth, validHeight, validMinCoords,
        validMaxCoords);
    Matrix2x2 transformMatrix = transform.getMatrix();
    Vector2D transformVector = transform.getVector();

    assertEquals(expectedA00, transformMatrix.getA00());
    assertEquals(expectedA01, transformMatrix.getA01());
    assertEquals(expectedA10, transformMatrix.getA10());
    assertEquals(expectedA11, transformMatrix.getA11());
    assertEquals(expectedB0, transformVector.getX0());
    assertEquals(expectedB1, transformVector.getX1());
  }

  @Test
  @DisplayName("Test clear-method")
  public void testClearMethod() {
    validCanvas.clear();
    
    Arrays.stream(validCanvas.getCanvasArray()).forEach(row ->
        Arrays.stream(row).forEach(element -> assertEquals(0, element)));
      }
}
