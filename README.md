# Portfolio project IDATT2003 - 2024
STUDENT 1 ID = 10042, 
STUDENT 2 ID = 10011

## Project description
This product is a result of a portfolio assessment in subject IDATT2003 at NTNU. It is an application to play chaos games, which are visualizations of fractals after a given amount of iterations.

## Project structure
The application may be found in src/main/java/edu/ntnu/idatt2003. JUnit-test-classes may be found in src/test/java/edu/ntnu/idatt2003.

## Link to repository
[Repository](https://gitlab.stud.idi.ntnu.no/carinemr/idatt2003_2024_gr53)

## How to run the project
For running the application, you should firstly clone the project to your computer on your chosen IDE. The application was made with Intellij IDEA, so this is recommended. You should also make sure to have Java SDK version 21 or greater, as well as JavaFX 21.0.1 or greater.


## How to run the tests
Locate the test-classes in src/test/java/edu/ntnu/idatt2003 and run these. You may run all of the tests at once, or run them
one by one.

## References
[ChatGPT](https://chat.openai.com/)

